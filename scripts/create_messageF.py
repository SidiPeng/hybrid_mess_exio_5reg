"""Create Message Extension Table

This script create the extension matrix F of the forground MessageEU20 system.
Therefore, the results of MessageEU20 are used in the IAMC format (pyam).  
"""

from pathlib import Path
import os

input_folder = Path(__file__).parent / "../input_data/MESSAGE"
output_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

import hybridvut.preprocessing as pp
import numpy as np
import pandas as pd
import pyam

# Cteate VUT for Message_ENGAGE_SSP2
model_type = "nested"
report_type = "default"
model = "ENGAGE_SSP2_v4.1.2_R12"

year_to_consider = 2020
scenario = "baseline"

regions = {
    "NOR": "R11_NOR",
    "AFR": "R11_AFR",
    "CPA": "R11_CPA",
    "EEU": "R11_EEU",
    "FSU": "R11_FSU",
    "LAM": "R11_LAM",
    "MEA": "R11_MEA",
    "NAM": "R11_NAM",
    "PAO": "R11_PAO",
    "PAS": "R11_PAS",
    "SAS": "R11_SAS",
    "WEU": "R11_WEU",
}

variables = ["emis|CO2|*", "emis|CH4|*", "emis|N2O|*"]

U = pd.read_excel(
    output_folder / "U_Mess_mon_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)
multiindex = U.columns

df_pyam = pyam.IamDataFrame(
    input_folder / "report_default_ENGAGE_SSP2_v4.1.2_R12_baseline_2020.csv"
)

F = pp.F_messageix_from_IAMC(
    df=df_pyam,
    model=model,
    scenario=scenario,
    year=year_to_consider,
    variables=variables,
    mapping_regions=regions,
    multiindex=multiindex,
)

# Delete values for export and import processes
exp_imp_techs = [
    "LNG_exp|M1",
    "LNG_imp|M1",
    "coal_exp|M1",
    "coal_imp|M1",
    "elec_exp|M1",
    "elec_imp|M1",
    "eth_exp|M1",
    "eth_imp|M1",
    "foil_exp|M1",
    "foil_imp|M1",
    "loil_exp|M1",
    "loil_imp|M1",
    "oil_exp|M1",
    "oil_imp|M1",
    "gas_exp|M1",
    "gas_imp|M1",
    "gas_exp_afr|M1",
    "gas_exp_cpa|M1",
    "gas_exp_eeu|M1",
    "gas_exp_nam|M1",
    "gas_exp_pao|M1",
    "gas_exp_sas|M1",
    "gas_exp_weu|M1",
]

F = F.transpose()
F[F.index.isin(exp_imp_techs, level=1)] = 0.0
F = F.transpose()

F.to_excel(output_folder / "F_Mess_2020.xlsx", index_label="intervention")
