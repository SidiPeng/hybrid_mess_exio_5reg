from pathlib import Path
import os

result_folder = Path(__file__).parent / "../output_results"
data_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(result_folder):
    os.makedirs(result_folder)

if not os.path.exists(data_folder):
    os.makedirs(data_folder)

import hybridvut as hyb
import pandas as pd


# HYBRID

V = pd.read_excel(
    result_folder / "V_hybridized_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
U = pd.read_excel(
    result_folder / "U_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1, 2],
)
F = pd.read_excel(
    result_folder / "F_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


VUT_total = hyb.VUT(V=V, U=U, F=F)

# Intervention-multipliers:
F_mult = VUT_total.calc_F_mult(
    tech_assumption="industry", raw_factors="per-ind", multipliers="per-com"
)

F_mult.to_excel(result_folder / "F_mult_com_2020.xlsx")

F_mult_ind = VUT_total.calc_F_mult(
    tech_assumption="industry", raw_factors="per-ind", multipliers="per-ind"
)

F_mult_ind.to_excel(result_folder / "F_mult_ind_2020.xlsx")


# EXIOBASE

V = pd.read_excel(
    data_folder / "V_back_agg_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
U = pd.read_excel(
    data_folder / "U_back_agg_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1, 2],
)
F = pd.read_excel(
    data_folder / "F_Exio_conv_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)

VUT_total = hyb.VUT(V=V, U=U, F=F)

# Intervention-multipliers:
F_mult = VUT_total.calc_F_mult(
    tech_assumption="industry", raw_factors="per-ind", multipliers="per-com"
)

F_mult.to_excel(result_folder / "F_mult_com_EXIO_2020.xlsx")

F_mult_ind = VUT_total.calc_F_mult(
    tech_assumption="industry", raw_factors="per-ind", multipliers="per-ind"
)

F_mult_ind.to_excel(result_folder / "F_mult_ind_EXIO_2020.xlsx")
