"""Create Message Make and Use Tables

This script create the forground sytem of MessageEU20.
Therefore, the results of MessageEU20 are used in the IAMC format (pyam). 
"""

from pathlib import Path
import os

input_folder = Path(__file__).parent / "../input_data/MESSAGE"
output_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

import hybridvut.preprocessing as pp
import numpy as np
import pandas as pd
import pyam

# Define model parameters
model = "ENGAGE_SSP2_v4.1.2_R12"
year_to_consider = 2020
scenario = "baseline"

# Cteate VUT for Message
df_pyam = pyam.IamDataFrame(
    input_folder / "report_default_ENGAGE_SSP2_v4.1.2_R12_baseline_2020.csv"
)

regions = {
    "NOR": ["R11_NOR|R11_GLB", "R11_NOR|R11_NOR"],
    "AFR": ["R11_AFR|R11_AFR", "R11_AFR|R11_GLB"],
    "CPA": ["R11_CPA|R11_CPA", "R11_CPA|R11_GLB"],
    "EEU": ["R11_EEU|R11_EEU", "R11_EEU|R11_GLB"],
    "FSU": ["R11_FSU|R11_FSU", "R11_FSU|R11_GLB"],
    "LAM": ["R11_LAM|R11_LAM", "R11_LAM|R11_GLB"],
    "MEA": ["R11_MEA|R11_MEA", "R11_MEA|R11_GLB"],
    "NAM": ["R11_NAM|R11_NAM", "R11_NAM|R11_GLB"],
    "PAO": ["R11_PAO|R11_PAO", "R11_PAO|R11_GLB"],
    "PAS": ["R11_PAS|R11_PAS", "R11_PAS|R11_GLB"],
    "SAS": ["R11_SAS|R11_SAS", "R11_SAS|R11_GLB"],
    "WEU": ["R11_WEU|R11_WEU", "R11_WEU|R11_GLB"],
}
#V-make,out, U-use,in
variables_V = {
    "V": [
        "out|useful|*",
        "out|useful|*",
        "out|primary|*",
        "out|secondary|*",
        "out|final|*",
        "out|export|*",
        "out|piped-gas|*",
    ],
}
variables_U = {
    "U": [
        "in|useful|*",
        "in|useful|*",
        "in|primary|*",
        "in|secondary|*",
        "in|final|*",
        "in|import|*",
        "in|piped-gas|*",
    ],
}

flows_not_needed = [
    "dom_total|M1",
    "exp_total|M1",
    "oil_extr_mpen|M1",
    "coal_extr_mpen|M1",
    "gas_extr_mpen|M1",
    "cement_CO2|M1",
    "flaring_CO2|M1",
    "imp_total|M1",
    "N2Oo_TCE|M1",
    "HFCo_TCE|M1",
]

# Calculate Make and Use matrices
V, U = pp.VUT_messageix_from_IAMC(
    df_pyam=df_pyam,
    model=model,
    scenario=scenario,
    year=year_to_consider,
    mapping_regions=regions,
    variables_V=variables_V,
    variables_U=variables_U,
    flows_not_needed=flows_not_needed,
    harmonize_inds_coms=True,
)
# before that remove piped-gas|gas manually
V.drop("piped-gas|gas", level="commodity", axis=1, inplace=True)
U.drop("piped-gas|gas", level="commodity", axis=0, inplace=True)

y = pp.y_messageix_from_V_U(V, U)

b = pp.balance_check_messageix(V, U, y)

# b.to_excel(output_folder / "trade_bal_Mess.xlsx")
V.to_excel(output_folder / "V_Mess_2020.xlsx")
U.to_excel(output_folder / "U_Mess_2020.xlsx")
