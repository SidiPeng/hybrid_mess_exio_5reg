""" This script shows the aggregation of regions, and
the conversion of units.
"""

from pathlib import Path
import os

output_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

import hybridvut as hyb
import pandas as pd

# 0) Read data
# Messageix (already monetized)
V_for = pd.read_excel(
    output_folder / "V_for_unit_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1, 2],
)
U_for = pd.read_excel(
    output_folder / "U_for_unit_2020.xlsx",
    index_col=[0, 1, 2],
    header=[0, 1],
)
F_for = pd.read_excel(
    output_folder / "F_for_unit_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)

# Exiobase
V_back = pd.read_excel(
    output_folder / "V_back_unit_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1, 2],
)
U_back = pd.read_excel(
    output_folder / "U_back_unit_2020.xlsx",
    index_col=[0, 1, 2],
    header=[0, 1],
)
F_back = pd.read_excel(
    output_folder / "F_back_unit_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)
F_Y_back = pd.read_excel(
    output_folder / "F_Y_back_unit_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


# 1) Create VUTs
VUT_back = hyb.VUT(V=V_back, U=U_back, F=F_back, F_Y=F_Y_back)
VUT_for = hyb.VUT(V=V_for, U=U_for, F=F_for)

# 2a) Aggregate regions
mapping_reg_for = {
    "EUR": ["WEU", "EEU", "FSU", "NOR"],
    "NAM": ["NAM"],
    "LAM": ["LAM"],
    "AFR_ME": ["AFR", "MEA"],
    "ASI_OZ": ["CPA", "SAS", "PAS", "PAO"],
    "Trade": ["Trade"],
}

mapping_reg_back = {
    "europe": [
        "AT",
        "BE",
        "BG",
        "CY",
        "CZ",
        "DE",
        "DK",
        "EE",
        "ES",
        "FI",
        "FR",
        "GR",
        "HR",
        "HU",
        "IE",
        "IT",
        "LT",
        "LU",
        "LV",
        "MT",
        "NL",
        "NO",
        "PL",
        "PT",
        "RO",
        "SE",
        "SI",
        "SK",
        "GB",
        "RU",
        "CH",
        "TR",
        "WE",
    ],
    "north_america": ["US", "CA"],
    "latin_america": ["BR", "MX", "WL"],
    "africa_mid_east": ["ZA", "WF", "WM"],
    "asia_ozeania": ["JP", "CN", "KR", "IN", "AU", "TW", "ID", "WA"],
}

# 2b) aggregate commodities
mapping_com_for = {
    "export|LNG": ["export|LNG"],
    "export|coal": ["export|coal"],
    "export|crudeoil": ["export|crudeoil"],
    "export|electr": ["export|electr"],
    "export|ethanol": ["export|ethanol"],
    "export|fueloil": ["export|fueloil"],
    "export|lightoil": ["export|lightoil"],
    "final|biomass": ["final|biomass"],
    "final|coal": ["final|coal"],
    "final|d_heat": ["final|d_heat"],
    "final|electr": ["final|electr"],
    "final|ethanol": ["final|ethanol"],
    "final|fueloil": ["final|fueloil"],
    "final|gas": ["final|gas"],
    "final|hydrogen": ["final|hydrogen"],
    "final|lh2": ["final|lh2"],
    "final|lightoil": ["final|lightoil"],
    "final|methanol": ["final|methanol"],
    "import|LNG": ["import|LNG"],
    "import|coal": ["import|coal"],
    "import|crudeoil": ["import|crudeoil"],
    "import|electr": ["import|electr"],
    "import|ethanol": ["import|ethanol"],
    "import|fueloil": ["import|fueloil"],
    "import|lightoil": ["import|lightoil"],
    "piped-gas|gas_afr_me": ["piped-gas|gas_afr"],
    "piped-gas|gas_asi_oz": [
        "piped-gas|gas_cpa",
        "piped-gas|gas_sas",
        "piped-gas|gas_pao",
    ],
    "piped-gas|gas_eur": ["piped-gas|gas_eeu", "piped-gas|gas_weu"],
    "piped-gas|gas_nam": ["piped-gas|gas_nam"],
    "primary|LNG": ["primary|LNG"],
    "primary|biomass": ["primary|biomass"],
    "primary|coal": ["primary|coal"],
    "primary|crudeoil": ["primary|crudeoil"],
    "primary|dumagr": ["primary|dumagr"],
    "primary|ethanol": ["primary|ethanol"],
    "primary|gas": ["primary|gas"],
    "primary|lh2": ["primary|lh2"],
    "primary|methanol": ["primary|methanol"],
    "secondary|LNG": ["secondary|LNG"],
    "secondary|coal": ["secondary|coal"],
    "secondary|crudeoil": ["secondary|crudeoil"],
    "secondary|d_heat": ["secondary|d_heat"],
    "secondary|electr": ["secondary|electr"],
    "secondary|ethanol": ["secondary|ethanol"],
    "secondary|exports": ["secondary|exports"],
    "secondary|fueloil": ["secondary|fueloil"],
    "secondary|gas": ["secondary|gas"],
    "secondary|hydrogen": ["secondary|hydrogen"],
    "secondary|lh2": ["secondary|lh2"],
    "secondary|lightoil": ["secondary|lightoil"],
    "secondary|methanol": ["secondary|methanol"],
    "secondary|u5": ["secondary|u5"],
    "useful|i_feed": ["useful|i_feed"],
    "useful|i_spec": ["useful|i_spec"],
    "useful|i_therm": ["useful|i_therm"],
    "useful|non-comm": ["useful|non-comm"],
    "useful|rc_spec": ["useful|rc_spec"],
    "useful|rc_therm": ["useful|rc_therm"],
    "useful|transport": ["useful|transport"],
}

mapping_ind_for = {
    "Feeds_1|M1": ["Feeds_1|M1"],
    "Feeds_2|M1": ["Feeds_2|M1"],
    "Feeds_3|M1": ["Feeds_3|M1"],
    "Feeds_4|M1": ["Feeds_4|M1"],
    "Feeds_5|M1": ["Feeds_5|M1"],
    "Feeds_con|M1": ["Feeds_con|M1"],
    "Ispec_1|M1": ["Ispec_1|M1"],
    "Ispec_2|M1": ["Ispec_2|M1"],
    "Ispec_3|M1": ["Ispec_3|M1"],
    "Ispec_4|M1": ["Ispec_4|M1"],
    "Ispec_5|M1": ["Ispec_5|M1"],
    "Ispec_con|M1": ["Ispec_con|M1"],
    "Itherm_1|M1": ["Itherm_1|M1"],
    "Itherm_2|M1": ["Itherm_2|M1"],
    "Itherm_3|M1": ["Itherm_3|M1"],
    "Itherm_4|M1": ["Itherm_4|M1"],
    "Itherm_5|M1": ["Itherm_5|M1"],
    "Itherm_con|M1": ["Itherm_con|M1"],
    "LNG_bal|M1": ["LNG_bal|M1"],
    "LNG_exp|M1": ["LNG_exp|M1"],
    "LNG_imp|M1": ["LNG_imp|M1"],
    "LNG_prod|M1": ["LNG_prod|M1"],
    "LNG_regas|M1": ["LNG_regas|M1"],
    "LNG_trade|M1": ["LNG_trade|M1"],
    "RCspec_1|M1": ["RCspec_1|M1"],
    "RCspec_2|M1": ["RCspec_2|M1"],
    "RCspec_3|M1": ["RCspec_3|M1"],
    "RCspec_4|M1": ["RCspec_4|M1"],
    "RCspec_5|M1": ["RCspec_5|M1"],
    "RCspec_con|M1": ["RCspec_con|M1"],
    "RCtherm_1|M1": ["RCtherm_1|M1"],
    "RCtherm_2|M1": ["RCtherm_2|M1"],
    "RCtherm_3|M1": ["RCtherm_3|M1"],
    "RCtherm_4|M1": ["RCtherm_4|M1"],
    "RCtherm_5|M1": ["RCtherm_5|M1"],
    "RCtherm_con|M1": ["RCtherm_con|M1"],
    "SO2_scrub_ind|M1": ["SO2_scrub_ind|M1"],
    "SO2_scrub_ppl|M1": ["SO2_scrub_ppl|M1"],
    "SO2_scrub_ref|M1": ["SO2_scrub_ref|M1"],
    "SO2_scrub_synf|M1": ["SO2_scrub_synf|M1"],
    "Trans_1|M1": ["Trans_1|M1"],
    "Trans_2|M1": ["Trans_2|M1"],
    "Trans_3|M1": ["Trans_3|M1"],
    "Trans_4|M1": ["Trans_4|M1"],
    "Trans_5|M1": ["Trans_5|M1"],
    "Trans_con|M1": ["Trans_con|M1"],
    "bco2_tr_dis|M1": ["bco2_tr_dis|M1"],
    "bio_extr_mpen|M1": ["bio_extr_mpen|M1"],
    "bio_hpl__air|M1": ["bio_hpl__air|M1"],
    "bio_hpl__cl_fresh|M1": ["bio_hpl__cl_fresh|M1"],
    "bio_hpl|M1": ["bio_hpl|M1"],
    "bio_istig__air|M1": ["bio_istig__air|M1"],
    "bio_istig__cl_fresh|M1": ["bio_istig__cl_fresh|M1"],
    "bio_istig|M1": ["bio_istig|M1"],
    "bio_ppl__air|M1": ["bio_ppl__air|M1"],
    "bio_ppl__cl_fresh|M1": ["bio_ppl__cl_fresh|M1"],
    "bio_ppl|M1": ["bio_ppl|M1"],
    "biomass_i|M1": ["biomass_i|M1"],
    "biomass_nc|M1": ["biomass_nc|M1"],
    "biomass_rc|M1": ["biomass_rc|M1"],
    "biomass_t_d|M1": ["biomass_t_d|M1"],
    "c_ppl_co2scr|M1": ["c_ppl_co2scr|M1"],
    "cement_co2scr|M1": ["cement_co2scr|M1"],
    "co2_tr_dis|M1": ["co2_tr_dis|M1"],
    "coal_adv__air|M1": ["coal_adv__air|M1"],
    "coal_adv__cl_fresh|M1": ["coal_adv__cl_fresh|M1"],
    "coal_adv_ccs__cl_fresh|M1": ["coal_adv_ccs__cl_fresh|M1"],
    "coal_adv_ccs|M1": ["coal_adv_ccs|M1"],
    "coal_adv|M1": ["coal_adv|M1"],
    "coal_bal|M1": ["coal_bal|M1"],
    "coal_exp|M1": ["coal_exp|M1"],
    "coal_extr_ch4|M1": ["coal_extr_ch4|M1"],
    "coal_extr|M1": ["coal_extr|M1"],
    "coal_fs|M1": ["coal_fs|M1"],
    "coal_gas|M1": ["coal_gas|M1"],
    "coal_hpl|M1": ["coal_hpl|M1"],
    "coal_imp|M1": ["coal_imp|M1"],
    "coal_i|M1": ["coal_i|M1"],
    "coal_ppl__air|M1": ["coal_ppl__air|M1"],
    "coal_ppl__cl_fresh|M1": ["coal_ppl__cl_fresh|M1"],
    "coal_ppl_u__air|M1": ["coal_ppl_u__air|M1"],
    "coal_ppl_u__cl_fresh|M1": ["coal_ppl_u__cl_fresh|M1"],
    "coal_ppl_u|M1": ["coal_ppl_u|M1"],
    "coal_ppl|M1": ["coal_ppl|M1"],
    "coal_rc|M1": ["coal_rc|M1"],
    "coal_std|M1": ["coal_std|M1"],
    "coal_t_d-in-06p|M1": ["coal_t_d-in-06p|M1"],
    "coal_t_d-rc-06p|M1": ["coal_t_d-rc-06p|M1"],
    "coal_t_d-rc-SO2|M1": ["coal_t_d-rc-SO2|M1"],
    "coal_t_d|M1": ["coal_t_d|M1"],
    "coal_trade|M1": ["coal_trade|M1"],
    "coal_trp|M1": ["coal_trp|M1"],
    "csp_sm1_ppl|M1": ["csp_sm1_ppl|M1"],
    "csp_sm1_res1|M1": ["csp_sm1_res1|M1"],
    "csp_sm1_res2|M1": ["csp_sm1_res2|M1"],
    "csp_sm1_res3|M1": ["csp_sm1_res3|M1"],
    "csp_sm1_res4|M1": ["csp_sm1_res4|M1"],
    "csp_sm1_res5|M1": ["csp_sm1_res5|M1"],
    "csp_sm1_res6|M1": ["csp_sm1_res6|M1"],
    "csp_sm1_res7|M1": ["csp_sm1_res7|M1"],
    "csp_sm1_res|M1": ["csp_sm1_res|M1"],
    "csp_sm3_ppl|M1": ["csp_sm3_ppl|M1"],
    "csp_sm3_res1|M1": ["csp_sm3_res1|M1"],
    "csp_sm3_res2|M1": ["csp_sm3_res2|M1"],
    "csp_sm3_res3|M1": ["csp_sm3_res3|M1"],
    "csp_sm3_res4|M1": ["csp_sm3_res4|M1"],
    "csp_sm3_res5|M1": ["csp_sm3_res5|M1"],
    "csp_sm3_res6|M1": ["csp_sm3_res6|M1"],
    "csp_sm3_res7|M1": ["csp_sm3_res7|M1"],
    "csp_sm3_res|M1": ["csp_sm3_res|M1"],
    "elec_exp|M1": ["elec_exp|M1"],
    "elec_imp|M1": ["elec_imp|M1"],
    "elec_i|M1": ["elec_i|M1"],
    "elec_rc|M1": ["elec_rc|M1"],
    "elec_t_d|M1": ["elec_t_d|M1"],
    "elec_trade|M1": ["elec_trade|M1"],
    "elec_trp|M1": ["elec_trp|M1"],
    "eth_bal|M1": ["eth_bal|M1"],
    "eth_bio|M1": ["eth_bio|M1"],
    "eth_exp|M1": ["eth_exp|M1"],
    "eth_fc_trp|M1": ["eth_fc_trp|M1"],
    "eth_ic_trp|M1": ["eth_ic_trp|M1"],
    "eth_imp|M1": ["eth_imp|M1"],
    "eth_i|M1": ["eth_i|M1"],
    "eth_rc|M1": ["eth_rc|M1"],
    "eth_t_d|M1": ["eth_t_d|M1"],
    "eth_trade|M1": ["eth_trade|M1"],
    "ethanol_fs|M1": ["ethanol_fs|M1"],
    "foil_exp|M1": ["foil_exp|M1"],
    "foil_fs|M1": ["foil_fs|M1"],
    "foil_hpl__air|M1": ["foil_hpl__air|M1"],
    "foil_hpl__cl_fresh|M1": ["foil_hpl__cl_fresh|M1"],
    "foil_hpl|M1": ["foil_hpl|M1"],
    "foil_imp|M1": ["foil_imp|M1"],
    "foil_i|M1": ["foil_i|M1"],
    "foil_ppl__air|M1": ["foil_ppl__air|M1"],
    "foil_ppl__cl_fresh|M1": ["foil_ppl__cl_fresh|M1"],
    "foil_ppl|M1": ["foil_ppl|M1"],
    "foil_rc|M1": ["foil_rc|M1"],
    "foil_t_d|M1": ["foil_t_d|M1"],
    "foil_trade|M1": ["foil_trade|M1"],
    "foil_trp|M1": ["foil_trp|M1"],
    "g_ppl_co2scr|M1": ["g_ppl_co2scr|M1"],
    "gas_bal|M1": ["gas_bal|M1"],
    "gas_bio|M1": ["gas_bio|M1"],
    "gas_cc__air|M1": ["gas_cc__air|M1"],
    "gas_cc__cl_fresh|M1": ["gas_cc__cl_fresh|M1"],
    "gas_cc_ccs__cl_fresh|M1": ["gas_cc_ccs__cl_fresh|M1"],
    "gas_cc_ccs|M1": ["gas_cc_ccs|M1"],
    "gas_cc|M1": ["gas_cc|M1"],
    "gas_ct|M1": ["gas_ct|M1"],
    "gas_exp_afr_me|M1": ["gas_exp_afr|M1"],
    "gas_exp_asi_oz|M1": ["gas_exp_cpa|M1", "gas_exp_sas|M1", "gas_exp_pao|M1"],
    "gas_exp_eur|M1": ["gas_exp_eeu|M1", "gas_exp_weu|M1"],
    "gas_exp_nam|M1": ["gas_exp_nam|M1"],
    "gas_extr_1|M1": ["gas_extr_1|M1"],
    "gas_extr_2|M1": ["gas_extr_2|M1"],
    "gas_extr_3|M1": ["gas_extr_3|M1"],
    "gas_extr_4|M1": ["gas_extr_4|M1"],
    "gas_extr_5|M1": ["gas_extr_5|M1"],
    "gas_extr_6|M1": ["gas_extr_6|M1"],
    "gas_fs|M1": ["gas_fs|M1"],
    "gas_hpl__air|M1": ["gas_hpl__air|M1"],
    "gas_hpl__cl_fresh|M1": ["gas_hpl__cl_fresh|M1"],
    "gas_hpl|M1": ["gas_hpl|M1"],
    "gas_imp|M1": ["gas_imp|M1"],
    "gas_i|M1": ["gas_i|M1"],
    "gas_ppl__air|M1": ["gas_ppl__air|M1"],
    "gas_ppl__cl_fresh|M1": ["gas_ppl__cl_fresh|M1"],
    "gas_ppl|M1": ["gas_ppl|M1"],
    "gas_rc|M1": ["gas_rc|M1"],
    "gas_sto|M1": ["gas_sto|M1"],
    "gas_t_d_ch4|M1": ["gas_t_d_ch4|M1"],
    "gas_t_d|M1": ["gas_t_d|M1"],
    "gas_trade_asi_oz|M1": ["gas_trade_cpa|M1", "gas_trade_sas|M1"],
    "gas_trade_eur|M1": ["gas_trade_eeu|M1", "gas_trade_weu|M1"],
    "gas_trp|M1": ["gas_trp|M1"],
    "geo_hpl__air|M1": ["geo_hpl__air|M1"],
    "geo_hpl__cl_fresh|M1": ["geo_hpl__cl_fresh|M1"],
    "geo_hpl|M1": ["geo_hpl|M1"],
    "geo_ppl__air|M1": ["geo_ppl__air|M1"],
    "geo_ppl__cl_fresh|M1": ["geo_ppl__cl_fresh|M1"],
    "geo_ppl|M1": ["geo_ppl|M1"],
    "h2_bio|M1": ["h2_bio|M1"],
    "h2_coal_ccs|M1": ["h2_coal_ccs|M1"],
    "h2_coal|M1": ["h2_coal|M1"],
    "h2_elec|M1": ["h2_elec|M1"],
    "h2_liq|M1": ["h2_liq|M1"],
    "h2_mix|M1": ["h2_mix|M1"],
    "h2_smr_ccs|M1": ["h2_smr_ccs|M1"],
    "h2_smr|M1": ["h2_smr|M1"],
    "h2_t_d|M1": ["h2_t_d|M1"],
    "heat_i|M1": ["heat_i|M1"],
    "heat_rc|M1": ["heat_rc|M1"],
    "heat_t_d|M1": ["heat_t_d|M1"],
    "hp_el_i|M1": ["hp_el_i|M1"],
    "hp_el_rc|M1": ["hp_el_rc|M1"],
    "hp_gas_i|M1": ["hp_gas_i|M1"],
    "hp_gas_rc|M1": ["hp_gas_rc|M1"],
    "hydro_hc|M1": ["hydro_hc|M1"],
    "hydro_lc|M1": ["hydro_lc|M1"],
    "igcc__air|M1": ["igcc__air|M1"],
    "igcc__cl_fresh|M1": ["igcc__cl_fresh|M1"],
    "igcc_ccs__cl_fresh|M1": ["igcc_ccs__cl_fresh|M1"],
    "igcc_ccs|M1": ["igcc_ccs|M1"],
    "igcc|M1": ["igcc|M1"],
    "land_use_biomass|M1": ["land_use_biomass|M1"],
    "landfill_digester1|M1": ["landfill_digester1|M1"],
    "landfill_direct1|M1": ["landfill_direct1|M1"],
    "landfill_ele|M1": ["landfill_ele|M1"],
    "landfill_heatprdn|M1": ["landfill_heatprdn|M1"],
    "lfil_con|M1": ["lfil_con|M1"],
    "lh2_bal|M1": ["lh2_bal|M1"],
    "lh2_t_d|M1": ["lh2_t_d|M1"],
    "lignite_extr|M1": ["lignite_extr|M1"],
    "liq_bio|M1": ["liq_bio|M1"],
    "loil_cc__air|M1": ["loil_cc__air|M1"],
    "loil_cc__cl_fresh|M1": ["loil_cc__cl_fresh|M1"],
    "loil_cc|M1": ["loil_cc|M1"],
    "loil_conv|M1": ["loil_conv|M1"],
    "loil_exp|M1": ["loil_exp|M1"],
    "loil_fs|M1": ["loil_fs|M1"],
    "loil_imp|M1": ["loil_imp|M1"],
    "loil_i|M1": ["loil_i|M1"],
    "loil_ppl__air|M1": ["loil_ppl__air|M1"],
    "loil_ppl__cl_fresh|M1": ["loil_ppl__cl_fresh|M1"],
    "loil_ppl|M1": ["loil_ppl|M1"],
    "loil_rc|M1": ["loil_rc|M1"],
    "loil_std|M1": ["loil_std|M1"],
    "loil_sto|M1": ["loil_sto|M1"],
    "loil_t_d|M1": ["loil_t_d|M1"],
    "loil_trade|M1": ["loil_trade|M1"],
    "loil_trp|M1": ["loil_trp|M1"],
    "meth_bal|M1": ["meth_bal|M1"],
    "meth_coal_ccs|M1": ["meth_coal_ccs|M1"],
    "meth_coal|M1": ["meth_coal|M1"],
    "meth_fc_trp|M1": ["meth_fc_trp|M1"],
    "meth_ic_trp|M1": ["meth_ic_trp|M1"],
    "meth_i|M1": ["meth_i|M1"],
    "meth_ng_ccs|M1": ["meth_ng_ccs|M1"],
    "meth_ng|M1": ["meth_ng|M1"],
    "meth_rc|M1": ["meth_rc|M1"],
    "meth_t_d|M1": ["meth_t_d|M1"],
    "methanol_fs|M1": ["methanol_fs|M1"],
    "nica_con|M1": ["nica_con|M1"],
    "nuc_hc__cl_fresh|M1": ["nuc_hc__cl_fresh|M1"],
    "nuc_hc|M1": ["nuc_hc|M1"],
    "nuc_lc__cl_fresh|M1": ["nuc_lc__cl_fresh|M1"],
    "nuc_lc|M1": ["nuc_lc|M1"],
    "oil_bal|M1": ["oil_bal|M1"],
    "oil_conv|M1": ["oil_conv|M1"],
    "oil_exp|M1": ["oil_exp|M1"],
    "oil_extr_1_ch4|M1": ["oil_extr_1_ch4|M1"],
    "oil_extr_1|M1": ["oil_extr_1|M1"],
    "oil_extr_2_ch4|M1": ["oil_extr_2_ch4|M1"],
    "oil_extr_2|M1": ["oil_extr_2|M1"],
    "oil_extr_3_ch4|M1": ["oil_extr_3_ch4|M1"],
    "oil_extr_3|M1": ["oil_extr_3|M1"],
    "oil_extr_4_ch4|M1": ["oil_extr_4_ch4|M1"],
    "oil_extr_4|M1": ["oil_extr_4|M1"],
    "oil_extr_5|M1": ["oil_extr_5|M1"],
    "oil_extr_6|M1": ["oil_extr_6|M1"],
    "oil_extr_7|M1": ["oil_extr_7|M1"],
    "oil_imp|M1": ["oil_imp|M1"],
    "oil_ppl|M1": ["oil_ppl|M1"],
    "oil_trade|M1": ["oil_trade|M1"],
    "po_turbine|M1": ["po_turbine|M1"],
    "ref_hil|M1": ["ref_hil|M1"],
    "ref_lol|M1": ["ref_lol|M1"],
    "solar_curtailment1|M1": ["solar_curtailment1|M1"],
    "solar_curtailment2|M1": ["solar_curtailment2|M1"],
    "solar_curtailment3|M1": ["solar_curtailment3|M1"],
    "solar_i|M1": ["solar_i|M1"],
    "solar_pv_ppl|M1": ["solar_pv_ppl|M1"],
    "solar_rc|M1": ["solar_rc|M1"],
    "solar_res1|M1": ["solar_res1|M1"],
    "solar_res2|M1": ["solar_res2|M1"],
    "solar_res3|M1": ["solar_res3|M1"],
    "solar_res4|M1": ["solar_res4|M1"],
    "solar_res5|M1": ["solar_res5|M1"],
    "solar_res6|M1": ["solar_res6|M1"],
    "solar_res7|M1": ["solar_res7|M1"],
    "solar_res8|M1": ["solar_res8|M1"],
    "solar_th_ppl__air|M1": ["solar_th_ppl__air|M1"],
    "solar_th_ppl__cl_fresh|M1": ["solar_th_ppl__cl_fresh|M1"],
    "solar_th_ppl|M1": ["solar_th_ppl|M1"],
    "sp_coal_I|M1": ["sp_coal_I|M1"],
    "sp_el_I|M1": ["sp_el_I|M1"],
    "sp_el_RC|M1": ["sp_el_RC|M1"],
    "sp_eth_I|M1": ["sp_eth_I|M1"],
    "sp_liq_I|M1": ["sp_liq_I|M1"],
    "sp_meth_I|M1": ["sp_meth_I|M1"],
    "stor_ppl|M1": ["stor_ppl|M1"],
    "syn_liq_ccs|M1": ["syn_liq_ccs|M1"],
    "syn_liq|M1": ["syn_liq|M1"],
    "wind_curtailment1|M1": ["wind_curtailment1|M1"],
    "wind_curtailment2|M1": ["wind_curtailment2|M1"],
    "wind_curtailment3|M1": ["wind_curtailment3|M1"],
    "wind_ppf|M1": ["wind_ppf|M1"],
    "wind_ppl|M1": ["wind_ppl|M1"],
    "wind_ref1|M1": ["wind_ref1|M1"],
    "wind_ref2|M1": ["wind_ref2|M1"],
    "wind_ref3|M1": ["wind_ref3|M1"],
    "wind_ref4|M1": ["wind_ref4|M1"],
    "wind_ref5|M1": ["wind_ref5|M1"],
    "wind_res1|M1": ["wind_res1|M1"],
    "wind_res2|M1": ["wind_res2|M1"],
    "wind_res3|M1": ["wind_res3|M1"],
    "wind_res4|M1": ["wind_res4|M1"],
}

VUT_for.aggregate(
    mapping_reg=mapping_reg_for,
    mapping_com=mapping_com_for,
    mapping_ind=mapping_ind_for,
)
VUT_back.aggregate(mapping_reg=mapping_reg_back)

# 3) Convert units to match in foreground and background systems
VUT_for.convert_unit(
    current="Mt C", to="kg", matrix="F", factor=44 / 12 * 1000 * 1000000
)
VUT_for.convert_unit(current="kt", to="kg", matrix="F")
VUT_back.convert_unit(current="M.EUR", to="EUR", matrix="F", factor=1000000)
VUT_back.convert_unit(current="M.EUR", to="EUR", matrix="F_Y", factor=1000000)
VUT_for.convert_unit(current="USD", to="EUR", matrix="U")
VUT_for.convert_unit(current="USD", to="EUR", matrix="V")


# 4) Write Tables
VUT_for.F.to_excel(output_folder / "F_Mess_conv_2020.xlsx")
VUT_back.F.to_excel(output_folder / "F_Exio_conv_2020.xlsx")

VUT_back.V.to_excel(output_folder / "V_back_agg_2020.xlsx")
VUT_for.V.to_excel(output_folder / "V_for_agg_2020.xlsx")

VUT_for.U.to_excel(output_folder / "U_for_agg_2020.xlsx")
VUT_back.U.to_excel(output_folder / "U_back_agg_2020.xlsx")

VUT_back.F_Y.to_excel(output_folder / "F_Y_Exio_conv_2020.xlsx")

# 5) Delete not needed tables
os.remove(output_folder / "F_back_unit_2020.xlsx")
os.remove(output_folder / "F_for_unit_2020.xlsx")
os.remove(output_folder / "F_Mess_2020.xlsx")
os.remove(output_folder / "F_Y_back_unit_2020.xlsx")
os.remove(output_folder / "U_back_unit_2020.xlsx")
os.remove(output_folder / "U_for_unit_2020.xlsx")
os.remove(output_folder / "U_Mess_2020.xlsx")
os.remove(output_folder / "U_Mess_mon_2020.xlsx")
os.remove(output_folder / "V_back_unit_2020.xlsx")
os.remove(output_folder / "V_for_unit_2020.xlsx")
os.remove(output_folder / "V_Mess_2020.xlsx")
os.remove(output_folder / "V_Mess_mon_2020.xlsx")
os.remove(output_folder / "p_Mess__2020.xlsx")

