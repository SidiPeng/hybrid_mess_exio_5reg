from pathlib import Path
import os

input_folder = Path(__file__).parent / "../output_results"
input_folder_1 = Path(__file__).parent / "../output_data"
output_folder = Path(__file__).parent / "../output_figures"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

import hybridvut as hyb
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Figures footprints

# Hybrid system

mapping_ind = {
    "BACK: Agriculture, forestry and fishing": [
        "Cultivation of paddy rice",
        "Cultivation of wheat",
        "Cultivation of cereal grains nec",
        "Cultivation of vegetables, fruit, nuts",
        "Cultivation of oil seeds",
        "Cultivation of sugar cane, sugar beet",
        "Cultivation of plant-based fibers",
        "Cultivation of crops nec",
        "Cattle farming",
        "Pigs farming",
        "Poultry farming",
        "Meat animals nec",
        "Animal products nec",
        "Raw milk",
        "Wool, silk-worm cocoons",
        "Manure treatment (conventional), storage and land application",
        "Manure treatment (biogas), storage and land application",
        "Forestry, logging and related service activities (02)",
        "Fishing, operating of fish hatcheries and fish farms; service activities incidental to fishing (05)",
    ],
    "BACK: Mining and quarrying": [
        "Mining of coal and lignite; extraction of peat (10)",
        "Extraction of crude petroleum and services related to crude oil extraction, excluding surveying",
        "Extraction of natural gas and services related to natural gas extraction, excluding surveying",
        "Extraction, liquefaction, and regasification of other petroleum and gaseous materials",
        "Mining of uranium and thorium ores (12)",
        "Mining of iron ores",
        "Mining of copper ores and concentrates",
        "Mining of nickel ores and concentrates",
        "Mining of aluminium ores and concentrates",
        "Mining of precious metal ores and concentrates",
        "Mining of lead, zinc and tin ores and concentrates",
        "Mining of other non-ferrous metal ores and concentrates",
        "Quarrying of stone",
        "Quarrying of sand and clay",
        "Mining of chemical and fertilizer minerals, production of salt, other mining and quarrying n.e.c.",
    ],
    "BACK: Processing and manufacturing": [
        "Processing of meat cattle",
        "Processing of meat pigs",
        "Processing of meat poultry",
        "Production of meat products nec",
        "Processing vegetable oils and fats",
        "Processing of dairy products",
        "Processed rice",
        "Sugar refining",
        "Processing of Food products nec",
        "Manufacture of beverages",
        "Manufacture of fish products",
        "Manufacture of tobacco products (16)",
        "Manufacture of textiles (17)",
        "Manufacture of wearing apparel; dressing and dyeing of fur (18)",
        "Tanning and dressing of leather; manufacture of luggage, handbags, saddlery, harness and footwear (19)",
        "Manufacture of wood and of products of wood and cork, except furniture; manufacture of articles of straw and plaiting materials (20)",
        "Re-processing of secondary wood material into new wood material",
        "Pulp",
        "Re-processing of secondary paper into new pulp",
        "Paper",
        "Publishing, printing and reproduction of recorded media (22)",
        "Manufacture of coke oven products",
        "Petroleum Refinery",
        "Processing of nuclear fuel",
        "Plastics, basic",
        "Re-processing of secondary plastic into new plastic",
        "N-fertiliser",
        "P- and other fertiliser",
        "Chemicals nec",
        "Manufacture of rubber and plastic products (25)",
        "Manufacture of glass and glass products",
        "Re-processing of secondary glass into new glass",
        "Manufacture of ceramic goods",
        "Manufacture of bricks, tiles and construction products, in baked clay",
        "Manufacture of cement, lime and plaster",
        "Re-processing of ash into clinker",
        "Manufacture of other non-metallic mineral products n.e.c.",
        "Manufacture of basic iron and steel and of ferro-alloys and first products thereof",
        "Re-processing of secondary steel into new steel",
        "Precious metals production",
        "Re-processing of secondary preciuos metals into new preciuos metals",
        "Aluminium production",
        "Re-processing of secondary aluminium into new aluminium",
        "Lead, zinc and tin production",
        "Re-processing of secondary lead into new lead, zinc and tin",
        "Copper production",
        "Re-processing of secondary copper into new copper",
        "Other non-ferrous metal production",
        "Re-processing of secondary other non-ferrous metals into new other non-ferrous metals",
        "Casting of metals",
        "Manufacture of fabricated metal products, except machinery and equipment (28)",
        "Manufacture of machinery and equipment n.e.c. (29)",
        "Manufacture of office machinery and computers (30)",
        "Manufacture of electrical machinery and apparatus n.e.c. (31)",
        "Manufacture of radio, television and communication equipment and apparatus (32)",
        "Manufacture of medical, precision and optical instruments, watches and clocks (33)",
        "Manufacture of motor vehicles, trailers and semi-trailers (34)",
        "Manufacture of other transport equipment (35)",
        "Manufacture of furniture; manufacturing n.e.c. (36)",
        "Recycling of waste and scrap",
        "Recycling of bottles by direct reuse",
    ],
    "BACK: Electricity generation": [
        "Production of electricity by coal",
        "Production of electricity by gas",
        "Production of electricity by nuclear",
        "Production of electricity by hydro",
        "Production of electricity by wind",
        "Production of electricity by petroleum and other oil derivatives",
        "Production of electricity by biomass and waste",
        "Production of electricity by solar photovoltaic",
        "Production of electricity by solar thermal",
        "Production of electricity by tide, wave, ocean",
        "Production of electricity by Geothermal",
        "Production of electricity nec",
        "Transmission of electricity",
        "Distribution and trade of electricity",
    ],
    "BACK: Heat and hot water supply": [
        "Manufacture of gas; distribution of gaseous fuels through mains",
        "Steam and hot water supply",
        "Collection, purification and distribution of water (41)",
    ],
    "BACK: Construction": [
        "Construction (45)",
        "Re-processing of secondary construction material into aggregates",
    ],
    "BACK: Administrative, support and service activities": [
        "Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries",
        "Retail sale of automotive fuel",
        "Wholesale trade and commission trade, except of motor vehicles and motorcycles (51)",
        "Retail trade, except of motor vehicles and motorcycles; repair of personal and household goods (52)",
        "Hotels and restaurants (55)",
        "Supporting and auxiliary transport activities; activities of travel agencies (63)",
        "Post and telecommunications (64)",
        "Financial intermediation, except insurance and pension funding (65)",
        "Insurance and pension funding, except compulsory social security (66)",
        "Activities auxiliary to financial intermediation (67)",
        "Real estate activities (70)",
        "Renting of machinery and equipment without operator and of personal and household goods (71)",
        "Computer and related activities (72)",
        "Research and development (73)",
        "Other business activities (74)",
        "Public administration and defence; compulsory social security (75)",
        "Education (80)",
        "Health and social work (85)",
        "Activities of membership organisation n.e.c. (91)",
        "Recreational, cultural and sporting activities (92)",
        "Other service activities (93)",
        "Private households with employed persons (95)",
        "Extra-territorial organizations and bodies",
    ],
    "BACK: Transportaion": [
        "Transport via railways",
        "Other land transport",
        "Transport via pipelines",
        "Sea and coastal water transport",
        "Inland water transport",
        "Air transport (62)",
    ],
    "BACK: Waste treatement": [
        "Incineration of waste: Food",
        "Incineration of waste: Paper",
        "Incineration of waste: Plastic",
        "Incineration of waste: Metals and Inert materials",
        "Incineration of waste: Textiles",
        "Incineration of waste: Wood",
        "Incineration of waste: Oil/Hazardous waste",
        "Biogasification of food waste, incl. land application",
        "Biogasification of paper, incl. land application",
        "Biogasification of sewage slugde, incl. land application",
        "Composting of food waste, incl. land application",
        "Composting of paper and wood, incl. land application",
        "Waste water treatment, food",
        "Waste water treatment, other",
        "Landfill of waste: Food",
        "Landfill of waste: Paper",
        "Landfill of waste: Plastic",
        "Landfill of waste: Inert/metal/hazardous",
        "Landfill of waste: Textiles",
        "Landfill of waste: Wood",
    ],
    "FOR: Minining and quarrying": [
        "coal_extr_ch4|M1",
        "coal_extr|M1",
        "gas_extr_1|M1",
        "gas_extr_2|M1",
        "gas_extr_3|M1",
        "gas_extr_4|M1",
        "gas_extr_5|M1",
        "gas_extr_6|M1",
        "lignite_extr|M1",
        "oil_extr_1_ch4|M1",
        "oil_extr_1|M1",
        "oil_extr_2_ch4|M1",
        "oil_extr_2|M1",
        "oil_extr_3_ch4|M1",
        "oil_extr_3|M1",
        "oil_extr_4_ch4|M1",
        "oil_extr_4|M1",
        "oil_extr_5|M1",
        "oil_extr_6|M1",
        "oil_extr_7|M1",
    ],
    "FOR: Processing and manufacturing": [
        "SO2_scrub_ind|M1",
        "SO2_scrub_ppl|M1",
        "SO2_scrub_ref|M1",
        "SO2_scrub_synf|M1",
        "bco2_tr_dis|M1",
        "cement_co2scr|M1",
        "coal_gas|M1",
        "coal_t_d-in-06p|M1",
        "coal_t_d-rc-06p|M1",
        "coal_t_d-rc-SO2|M1",
        "coal_t_d|M1",
        "eth_bio|M1",
        "foil_t_d|M1",
        "gas_bio|M1",
        "gas_t_d_ch4|M1",
        "gas_t_d|M1",
        "h2_bio|M1",
        "h2_coal_ccs|M1",
        "h2_coal|M1",
        "h2_elec|M1",
        "h2_liq|M1",
        "h2_mix|M1",
        "h2_smr_ccs|M1",
        "h2_smr|M1",
        "h2_t_d|M1",
        "lh2_t_d|M1",
        "liq_bio|M1",
        "loil_t_d|M1",
        "meth_coal_ccs|M1",
        "meth_coal|M1",
        "meth_ng_ccs|M1",
        "meth_ng|M1",
        "meth_t_d|M1",
        "ref_hil|M1",
        "ref_lol|M1",
        "syn_liq_ccs|M1",
        "syn_liq|M1",
        "biomass_t_d|M1",
        "eth_t_d|M1",
    ],
    "FOR: Electricity generation": [
        "bio_istig__air|M1",
        "bio_istig__cl_fresh|M1",
        "bio_istig|M1",
        "bio_ppl__air|M1",
        "bio_ppl__cl_fresh|M1",
        "bio_ppl|M1",
        "c_ppl_co2scr|M1",
        "co2_tr_dis|M1",
        "coal_adv__air|M1",
        "coal_adv__cl_fresh|M1",
        "coal_adv_ccs__cl_fresh|M1",
        "coal_adv_ccs|M1",
        "coal_adv|M1",
        "coal_ppl__air|M1",
        "coal_ppl__cl_fresh|M1",
        "coal_ppl_u__air|M1",
        "coal_ppl_u__cl_fresh|M1",
        "coal_ppl_u|M1",
        "coal_ppl|M1",
        "csp_sm1_ppl|M1",
        "csp_sm1_res1|M1",
        "csp_sm1_res2|M1",
        "csp_sm1_res3|M1",
        "csp_sm1_res4|M1",
        "csp_sm1_res5|M1",
        "csp_sm1_res6|M1",
        "csp_sm1_res7|M1",
        "csp_sm1_res|M1",
        "csp_sm3_ppl|M1",
        "csp_sm3_res1|M1",
        "csp_sm3_res2|M1",
        "csp_sm3_res3|M1",
        "csp_sm3_res4|M1",
        "csp_sm3_res5|M1",
        "csp_sm3_res6|M1",
        "csp_sm3_res7|M1",
        "csp_sm3_res|M1",
        "elec_t_d|M1",
        "foil_ppl__air|M1",
        "foil_ppl__cl_fresh|M1",
        "foil_ppl|M1",
        "g_ppl_co2scr|M1",
        "gas_cc__air|M1",
        "gas_cc__cl_fresh|M1",
        "gas_cc_ccs__cl_fresh|M1",
        "gas_cc_ccs|M1",
        "gas_cc|M1",
        "gas_ct|M1",
        "gas_ppl__air|M1",
        "gas_ppl__cl_fresh|M1",
        "gas_ppl|M1",
        "hydro_hc|M1",
        "hydro_lc|M1",
        "igcc__air|M1",
        "igcc__cl_fresh|M1",
        "igcc_ccs__cl_fresh|M1",
        "igcc_ccs|M1",
        "igcc|M1",
        "loil_cc__air|M1",
        "loil_cc__cl_fresh|M1",
        "loil_cc|M1",
        "loil_ppl__air|M1",
        "loil_ppl__cl_fresh|M1",
        "loil_ppl|M1",
        "nuc_hc__cl_fresh|M1",
        "nuc_hc|M1",
        "nuc_lc__cl_fresh|M1",
        "nuc_lc|M1",
        "oil_ppl|M1",
        "solar_pv_ppl|M1",
        "solar_res1|M1",
        "solar_res2|M1",
        "solar_res3|M1",
        "solar_res4|M1",
        "solar_res5|M1",
        "solar_res6|M1",
        "solar_res7|M1",
        "solar_res8|M1",
        "solar_th_ppl__air|M1",
        "solar_th_ppl__cl_fresh|M1",
        "solar_th_ppl|M1",
        "stor_ppl|M1",
        "wind_ppf|M1",
        "wind_ppl|M1",
        "wind_ref1|M1",
        "wind_ref2|M1",
        "wind_ref3|M1",
        "wind_ref4|M1",
        "wind_ref5|M1",
        "wind_res1|M1",
        "wind_res2|M1",
        "wind_res3|M1",
        "wind_res4|M1",
    ],
    "FOR: Heat and hot water supply": [
        "bio_hpl__air|M1",
        "bio_hpl__cl_fresh|M1",
        "bio_hpl|M1",
        "coal_hpl|M1",
        "foil_hpl__air|M1",
        "foil_hpl__cl_fresh|M1",
        "foil_hpl|M1",
        "gas_hpl__air|M1",
        "gas_hpl__cl_fresh|M1",
        "gas_hpl|M1",
        "geo_hpl__air|M1",
        "geo_hpl__cl_fresh|M1",
        "geo_hpl|M1",
        "geo_ppl__air|M1",
        "geo_ppl__cl_fresh|M1",
        "geo_ppl|M1",
        "heat_t_d|M1",
        "Feeds_1|M1",
        "Feeds_2|M1",
        "Feeds_3|M1",
        "Feeds_4|M1",
        "Feeds_5|M1",
        "Feeds_con|M1",
        "Ispec_1|M1",
        "Ispec_2|M1",
        "Ispec_3|M1",
        "Ispec_4|M1",
        "Ispec_5|M1",
        "Ispec_con|M1",
        "Itherm_1|M1",
        "Itherm_2|M1",
        "Itherm_3|M1",
        "Itherm_4|M1",
        "Itherm_5|M1",
        "Itherm_con|M1",
        "LNG_bal|M1",
        "LNG_exp|M1",
        "LNG_imp|M1",
        "LNG_prod|M1",
        "LNG_regas|M1",
        "LNG_trade|M1",
        "RCspec_1|M1",
        "RCspec_2|M1",
        "RCspec_3|M1",
        "RCspec_4|M1",
        "RCspec_5|M1",
        "RCspec_con|M1",
        "RCtherm_1|M1",
        "RCtherm_2|M1",
        "RCtherm_3|M1",
        "RCtherm_4|M1",
        "RCtherm_5|M1",
        "RCtherm_con|M1",
        "Trans_1|M1",
        "Trans_2|M1",
        "Trans_3|M1",
        "Trans_4|M1",
        "Trans_5|M1",
        "Trans_con|M1",
        "bio_extr_mpen|M1",
        "biomass_i|M1",
        "biomass_nc|M1",
        "biomass_rc|M1",
        "coal_bal|M1",
        "coal_exp|M1",
        "coal_fs|M1",
        "coal_imp|M1",
        "coal_i|M1",
        "coal_rc|M1",
        "coal_std|M1",
        "coal_trade|M1",
        "coal_trp|M1",
        "elec_exp|M1",
        "elec_imp|M1",
        "elec_i|M1",
        "elec_rc|M1",
        "elec_trade|M1",
        "elec_trp|M1",
        "eth_bal|M1",
        "eth_exp|M1",
        "eth_fc_trp|M1",
        "eth_ic_trp|M1",
        "eth_imp|M1",
        "eth_i|M1",
        "eth_rc|M1",
        "eth_trade|M1",
        "ethanol_fs|M1",
        "foil_exp|M1",
        "foil_fs|M1",
        "foil_imp|M1",
        "foil_i|M1",
        "foil_rc|M1",
        "foil_trade|M1",
        "foil_trp|M1",
        "gas_bal|M1",
        "gas_exp_afr_me|M1",
        "gas_exp_asi_oz|M1",
        "gas_exp_eur|M1",
        "gas_exp_nam|M1",
        "gas_fs|M1",
        "gas_imp|M1",
        "gas_i|M1",
        "gas_rc|M1",
        "gas_sto|M1",
        "gas_trade_asi_oz|M1",
        "gas_trade_eur|M1",
        "gas_trp|M1",
        "heat_i|M1",
        "heat_rc|M1",
        "hp_el_i|M1",
        "hp_el_rc|M1",
        "hp_gas_i|M1",
        "hp_gas_rc|M1",
        "land_use_biomass|M1",
        "landfill_digester1|M1",
        "landfill_direct1|M1",
        "landfill_ele|M1",
        "landfill_heatprdn|M1",
        "lfil_con|M1",
        "lh2_bal|M1",
        "loil_conv|M1",
        "loil_exp|M1",
        "loil_fs|M1",
        "loil_imp|M1",
        "loil_i|M1",
        "loil_rc|M1",
        "loil_std|M1",
        "loil_sto|M1",
        "loil_trade|M1",
        "loil_trp|M1",
        "meth_bal|M1",
        "meth_fc_trp|M1",
        "meth_ic_trp|M1",
        "meth_i|M1",
        "meth_rc|M1",
        "methanol_fs|M1",
        "nica_con|M1",
        "oil_bal|M1",
        "oil_conv|M1",
        "oil_exp|M1",
        "oil_imp|M1",
        "oil_trade|M1",
        "po_turbine|M1",
        "solar_curtailment1|M1",
        "solar_curtailment2|M1",
        "solar_curtailment3|M1",
        "solar_i|M1",
        "solar_rc|M1",
        "sp_coal_I|M1",
        "sp_el_I|M1",
        "sp_el_RC|M1",
        "sp_eth_I|M1",
        "sp_liq_I|M1",
        "sp_meth_I|M1",
        "wind_curtailment1|M1",
        "wind_curtailment2|M1",
        "wind_curtailment3|M1",
    ],
}

S_ind = pd.DataFrame(
    0.0,
    index=mapping_ind.keys(),
    columns=sorted(set(v for val in mapping_ind.values() for v in val)),
)
for k, v in mapping_ind.items():
    S_ind.at[k, v] = 1.0

F = pd.read_excel(
    input_folder / "F_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)

F.drop("Trade", axis=1, inplace=True)

F = F.transpose().reset_index()
F["region"] = F["region"].str.replace("europe", "EUR")
F["region"] = F["region"].str.replace("north_america", "NAM")
F["region"] = F["region"].str.replace("latin_america", "LAM")
F["region"] = F["region"].str.replace("africa_mid_east", "AFR_ME")
F["region"] = F["region"].str.replace("asia_ozeania", "ASI_OZ")

F.set_index(["region", "industry"], inplace=True)
F = F.transpose()


reg = F.columns.levels[0].unique()
S_reg = pd.DataFrame(np.identity(len(reg)), index=reg, columns=reg)

I_i = pd.MultiIndex.from_product(
    [S_reg.index.tolist(), S_ind.index.tolist()], names=["region", "industry"]
)
I_c = pd.MultiIndex.from_product(
    [S_reg.columns.tolist(), S_ind.columns.tolist()],
    names=["region", "industry"],
)
S_I_hyb = pd.DataFrame(np.kron(S_reg, S_ind), index=I_i, columns=I_c)

F_agg = F.dot(S_I_hyb.transpose())


# Exiobase

mapping_ind = {
    "Agriculture, forestry and fishing": [
        "Cultivation of paddy rice",
        "Cultivation of wheat",
        "Cultivation of cereal grains nec",
        "Cultivation of vegetables, fruit, nuts",
        "Cultivation of oil seeds",
        "Cultivation of sugar cane, sugar beet",
        "Cultivation of plant-based fibers",
        "Cultivation of crops nec",
        "Cattle farming",
        "Pigs farming",
        "Poultry farming",
        "Meat animals nec",
        "Animal products nec",
        "Raw milk",
        "Wool, silk-worm cocoons",
        "Manure treatment (conventional), storage and land application",
        "Manure treatment (biogas), storage and land application",
        "Forestry, logging and related service activities (02)",
        "Fishing, operating of fish hatcheries and fish farms; service activities incidental to fishing (05)",
    ],
    "Mining and quarrying": [
        "Mining of coal and lignite; extraction of peat (10)",
        "Extraction of crude petroleum and services related to crude oil extraction, excluding surveying",
        "Extraction of natural gas and services related to natural gas extraction, excluding surveying",
        "Extraction, liquefaction, and regasification of other petroleum and gaseous materials",
        "Mining of uranium and thorium ores (12)",
        "Mining of iron ores",
        "Mining of copper ores and concentrates",
        "Mining of nickel ores and concentrates",
        "Mining of aluminium ores and concentrates",
        "Mining of precious metal ores and concentrates",
        "Mining of lead, zinc and tin ores and concentrates",
        "Mining of other non-ferrous metal ores and concentrates",
        "Quarrying of stone",
        "Quarrying of sand and clay",
        "Mining of chemical and fertilizer minerals, production of salt, other mining and quarrying n.e.c.",
    ],
    "Processing and manufacturing": [
        "Processing of meat cattle",
        "Processing of meat pigs",
        "Processing of meat poultry",
        "Production of meat products nec",
        "Processing vegetable oils and fats",
        "Processing of dairy products",
        "Processed rice",
        "Sugar refining",
        "Processing of Food products nec",
        "Manufacture of beverages",
        "Manufacture of fish products",
        "Manufacture of tobacco products (16)",
        "Manufacture of textiles (17)",
        "Manufacture of wearing apparel; dressing and dyeing of fur (18)",
        "Tanning and dressing of leather; manufacture of luggage, handbags, saddlery, harness and footwear (19)",
        "Manufacture of wood and of products of wood and cork, except furniture; manufacture of articles of straw and plaiting materials (20)",
        "Re-processing of secondary wood material into new wood material",
        "Pulp",
        "Re-processing of secondary paper into new pulp",
        "Paper",
        "Publishing, printing and reproduction of recorded media (22)",
        "Manufacture of coke oven products",
        "Petroleum Refinery",
        "Processing of nuclear fuel",
        "Plastics, basic",
        "Re-processing of secondary plastic into new plastic",
        "N-fertiliser",
        "P- and other fertiliser",
        "Chemicals nec",
        "Manufacture of rubber and plastic products (25)",
        "Manufacture of glass and glass products",
        "Re-processing of secondary glass into new glass",
        "Manufacture of ceramic goods",
        "Manufacture of bricks, tiles and construction products, in baked clay",
        "Manufacture of cement, lime and plaster",
        "Re-processing of ash into clinker",
        "Manufacture of other non-metallic mineral products n.e.c.",
        "Manufacture of basic iron and steel and of ferro-alloys and first products thereof",
        "Re-processing of secondary steel into new steel",
        "Precious metals production",
        "Re-processing of secondary preciuos metals into new preciuos metals",
        "Aluminium production",
        "Re-processing of secondary aluminium into new aluminium",
        "Lead, zinc and tin production",
        "Re-processing of secondary lead into new lead, zinc and tin",
        "Copper production",
        "Re-processing of secondary copper into new copper",
        "Other non-ferrous metal production",
        "Re-processing of secondary other non-ferrous metals into new other non-ferrous metals",
        "Casting of metals",
        "Manufacture of fabricated metal products, except machinery and equipment (28)",
        "Manufacture of machinery and equipment n.e.c. (29)",
        "Manufacture of office machinery and computers (30)",
        "Manufacture of electrical machinery and apparatus n.e.c. (31)",
        "Manufacture of radio, television and communication equipment and apparatus (32)",
        "Manufacture of medical, precision and optical instruments, watches and clocks (33)",
        "Manufacture of motor vehicles, trailers and semi-trailers (34)",
        "Manufacture of other transport equipment (35)",
        "Manufacture of furniture; manufacturing n.e.c. (36)",
        "Recycling of waste and scrap",
        "Recycling of bottles by direct reuse",
    ],
    "Electricity generation": [
        "Production of electricity by coal",
        "Production of electricity by gas",
        "Production of electricity by nuclear",
        "Production of electricity by hydro",
        "Production of electricity by wind",
        "Production of electricity by petroleum and other oil derivatives",
        "Production of electricity by biomass and waste",
        "Production of electricity by solar photovoltaic",
        "Production of electricity by solar thermal",
        "Production of electricity by tide, wave, ocean",
        "Production of electricity by Geothermal",
        "Production of electricity nec",
        "Transmission of electricity",
        "Distribution and trade of electricity",
    ],
    "Heat and water supply": [
        "Manufacture of gas; distribution of gaseous fuels through mains",
        "Steam and hot water supply",
        "Collection, purification and distribution of water (41)",
    ],
    "Construction": [
        "Construction (45)",
        "Re-processing of secondary construction material into aggregates",
    ],
    "Administrative, support and service activities": [
        "Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries",
        "Retail sale of automotive fuel",
        "Wholesale trade and commission trade, except of motor vehicles and motorcycles (51)",
        "Retail trade, except of motor vehicles and motorcycles; repair of personal and household goods (52)",
        "Hotels and restaurants (55)",
        "Supporting and auxiliary transport activities; activities of travel agencies (63)",
        "Post and telecommunications (64)",
        "Financial intermediation, except insurance and pension funding (65)",
        "Insurance and pension funding, except compulsory social security (66)",
        "Activities auxiliary to financial intermediation (67)",
        "Real estate activities (70)",
        "Renting of machinery and equipment without operator and of personal and household goods (71)",
        "Computer and related activities (72)",
        "Research and development (73)",
        "Other business activities (74)",
        "Public administration and defence; compulsory social security (75)",
        "Education (80)",
        "Health and social work (85)",
        "Activities of membership organisation n.e.c. (91)",
        "Recreational, cultural and sporting activities (92)",
        "Other service activities (93)",
        "Private households with employed persons (95)",
        "Extra-territorial organizations and bodies",
    ],
    "Transportation": [
        "Transport via railways",
        "Other land transport",
        "Transport via pipelines",
        "Sea and coastal water transport",
        "Inland water transport",
        "Air transport (62)",
    ],
    "Waste treatment": [
        "Incineration of waste: Food",
        "Incineration of waste: Paper",
        "Incineration of waste: Plastic",
        "Incineration of waste: Metals and Inert materials",
        "Incineration of waste: Textiles",
        "Incineration of waste: Wood",
        "Incineration of waste: Oil/Hazardous waste",
        "Biogasification of food waste, incl. land application",
        "Biogasification of paper, incl. land application",
        "Biogasification of sewage slugde, incl. land application",
        "Composting of food waste, incl. land application",
        "Composting of paper and wood, incl. land application",
        "Waste water treatment, food",
        "Waste water treatment, other",
        "Landfill of waste: Food",
        "Landfill of waste: Paper",
        "Landfill of waste: Plastic",
        "Landfill of waste: Inert/metal/hazardous",
        "Landfill of waste: Textiles",
        "Landfill of waste: Wood",
    ],
    "Demand": [
        "Final consumption expenditure by households",
        "Final consumption expenditure by non-profit organisations serving households (NPISH)",
        "Final consumption expenditure by government",
        "Gross fixed capital formation",
        "Changes in inventories",
        "Changes in valuables",
        "Exports: Total (fob)",
    ],
}

S_ind = pd.DataFrame(
    0.0,
    index=mapping_ind.keys(),
    columns=sorted(set(v for val in mapping_ind.values() for v in val)),
)
for k, v in mapping_ind.items():
    S_ind.at[k, v] = 1.0

F = pd.read_excel(
    input_folder / "footprint_production_sector_EXIO_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)

F = F.transpose().reset_index()
F["region"] = F["region"].str.replace("europe", "EUR")
F["region"] = F["region"].str.replace("north_america", "NAM")
F["region"] = F["region"].str.replace("latin_america", "LAM")
F["region"] = F["region"].str.replace("africa_mid_east", "AFR_ME")
F["region"] = F["region"].str.replace("asia_ozeania", "ASI_OZ")

F.set_index(["region", "industry"], inplace=True)
F = F.transpose()


reg = F.columns.levels[0].unique()
S_reg = pd.DataFrame(np.identity(len(reg)), index=reg, columns=reg)

I_i = pd.MultiIndex.from_product(
    [S_reg.index.tolist(), S_ind.index.tolist()], names=["region", "industry"]
)
I_c = pd.MultiIndex.from_product(
    [S_reg.columns.tolist(), S_ind.columns.tolist()],
    names=["region", "industry"],
)
S_I_exio = pd.DataFrame(np.kron(S_reg, S_ind), index=I_i, columns=I_c)

F_agg_exio = F.dot(S_I_exio.transpose())


# Messageix

mapping_ind = {
    "Coal": [
        "coal_extr_ch4|M1",
        "coal_extr|M1",
    ],
    "Gas": [
        "gas_extr_1|M1",
        "gas_extr_2|M1",
        "gas_extr_3|M1",
        "gas_extr_4|M1",
        "gas_extr_5|M1",
        "gas_extr_6|M1",
    ],
    "Lignite": [
        "lignite_extr|M1",
    ],
    "Oil": [
        "oil_extr_1_ch4|M1",
        "oil_extr_1|M1",
        "oil_extr_2_ch4|M1",
        "oil_extr_2|M1",
        "oil_extr_3_ch4|M1",
        "oil_extr_3|M1",
        "oil_extr_4_ch4|M1",
        "oil_extr_4|M1",
        "oil_extr_5|M1",
        "oil_extr_6|M1",
        "oil_extr_7|M1",
        "Feeds_1|M1",
        "Feeds_2|M1",
        "Feeds_3|M1",
        "Feeds_4|M1",
        "Feeds_5|M1",
        "Feeds_con|M1",
        "Ispec_1|M1",
        "Ispec_2|M1",
        "Ispec_3|M1",
        "Ispec_4|M1",
        "Ispec_5|M1",
        "Ispec_con|M1",
        "Itherm_1|M1",
        "Itherm_2|M1",
        "Itherm_3|M1",
        "Itherm_4|M1",
        "Itherm_5|M1",
        "Itherm_con|M1",
        "LNG_bal|M1",
        "LNG_exp|M1",
        "LNG_imp|M1",
        "LNG_prod|M1",
        "LNG_regas|M1",
        "LNG_trade|M1",
        "RCspec_1|M1",
        "RCspec_2|M1",
        "RCspec_3|M1",
        "RCspec_4|M1",
        "RCspec_5|M1",
        "RCspec_con|M1",
        "RCtherm_1|M1",
        "RCtherm_2|M1",
        "RCtherm_3|M1",
        "RCtherm_4|M1",
        "RCtherm_5|M1",
        "RCtherm_con|M1",
        "SO2_scrub_ind|M1",
        "SO2_scrub_ppl|M1",
        "SO2_scrub_ref|M1",
        "SO2_scrub_synf|M1",
        "Trans_1|M1",
        "Trans_2|M1",
        "Trans_3|M1",
        "Trans_4|M1",
        "Trans_5|M1",
        "Trans_con|M1",
        "bco2_tr_dis|M1",
        "bio_extr_mpen|M1",
        "bio_hpl__air|M1",
        "bio_hpl__cl_fresh|M1",
        "bio_hpl|M1",
        "bio_istig__air|M1",
        "bio_istig__cl_fresh|M1",
        "bio_istig|M1",
        "bio_ppl__air|M1",
        "bio_ppl__cl_fresh|M1",
        "bio_ppl|M1",
        "biomass_i|M1",
        "biomass_nc|M1",
        "biomass_rc|M1",
        "biomass_t_d|M1",
        "c_ppl_co2scr|M1",
        "cement_co2scr|M1",
        "co2_tr_dis|M1",
        "coal_adv__air|M1",
        "coal_adv__cl_fresh|M1",
        "coal_adv_ccs__cl_fresh|M1",
        "coal_adv_ccs|M1",
        "coal_adv|M1",
        "coal_bal|M1",
        "coal_exp|M1",
        "coal_fs|M1",
        "coal_gas|M1",
        "coal_hpl|M1",
        "coal_imp|M1",
        "coal_i|M1",
        "coal_ppl__air|M1",
        "coal_ppl__cl_fresh|M1",
        "coal_ppl_u__air|M1",
        "coal_ppl_u__cl_fresh|M1",
        "coal_ppl_u|M1",
        "coal_ppl|M1",
        "coal_rc|M1",
        "coal_std|M1",
        "coal_t_d-in-06p|M1",
        "coal_t_d-rc-06p|M1",
        "coal_t_d-rc-SO2|M1",
        "coal_t_d|M1",
        "coal_trade|M1",
        "coal_trp|M1",
        "csp_sm1_ppl|M1",
        "csp_sm1_res1|M1",
        "csp_sm1_res2|M1",
        "csp_sm1_res3|M1",
        "csp_sm1_res4|M1",
        "csp_sm1_res5|M1",
        "csp_sm1_res6|M1",
        "csp_sm1_res7|M1",
        "csp_sm1_res|M1",
        "csp_sm3_ppl|M1",
        "csp_sm3_res1|M1",
        "csp_sm3_res2|M1",
        "csp_sm3_res3|M1",
        "csp_sm3_res4|M1",
        "csp_sm3_res5|M1",
        "csp_sm3_res6|M1",
        "csp_sm3_res7|M1",
        "csp_sm3_res|M1",
        "elec_exp|M1",
        "elec_imp|M1",
        "elec_i|M1",
        "elec_rc|M1",
        "elec_t_d|M1",
        "elec_trade|M1",
        "elec_trp|M1",
        "eth_bal|M1",
        "eth_bio|M1",
        "eth_exp|M1",
        "eth_fc_trp|M1",
        "eth_ic_trp|M1",
        "eth_imp|M1",
        "eth_i|M1",
        "eth_rc|M1",
        "eth_t_d|M1",
        "eth_trade|M1",
        "ethanol_fs|M1",
        "foil_exp|M1",
        "foil_fs|M1",
        "foil_hpl__air|M1",
        "foil_hpl__cl_fresh|M1",
        "foil_hpl|M1",
        "foil_imp|M1",
        "foil_i|M1",
        "foil_ppl__air|M1",
        "foil_ppl__cl_fresh|M1",
        "foil_ppl|M1",
        "foil_rc|M1",
        "foil_t_d|M1",
        "foil_trade|M1",
        "foil_trp|M1",
        "g_ppl_co2scr|M1",
        "gas_bal|M1",
        "gas_bio|M1",
        "gas_cc__air|M1",
        "gas_cc__cl_fresh|M1",
        "gas_cc_ccs__cl_fresh|M1",
        "gas_cc_ccs|M1",
        "gas_cc|M1",
        "gas_ct|M1",
        "gas_exp_afr_me|M1",
        "gas_exp_asi_oz|M1",
        "gas_exp_eur|M1",
        "gas_exp_nam|M1",
        "gas_fs|M1",
        "gas_hpl__air|M1",
        "gas_hpl__cl_fresh|M1",
        "gas_hpl|M1",
        "gas_imp|M1",
        "gas_i|M1",
        "gas_ppl__air|M1",
        "gas_ppl__cl_fresh|M1",
        "gas_ppl|M1",
        "gas_rc|M1",
        "gas_sto|M1",
        "gas_t_d_ch4|M1",
        "gas_t_d|M1",
        "gas_trade_asi_oz|M1",
        "gas_trade_eur|M1",
        "gas_trp|M1",
        "geo_hpl__air|M1",
        "geo_hpl__cl_fresh|M1",
        "geo_hpl|M1",
        "geo_ppl__air|M1",
        "geo_ppl__cl_fresh|M1",
        "geo_ppl|M1",
        "h2_bio|M1",
        "h2_coal_ccs|M1",
        "h2_coal|M1",
        "h2_elec|M1",
        "h2_liq|M1",
        "h2_mix|M1",
        "h2_smr_ccs|M1",
        "h2_smr|M1",
        "h2_t_d|M1",
        "heat_i|M1",
        "heat_rc|M1",
        "heat_t_d|M1",
        "hp_el_i|M1",
        "hp_el_rc|M1",
        "hp_gas_i|M1",
        "hp_gas_rc|M1",
        "hydro_hc|M1",
        "hydro_lc|M1",
        "igcc__air|M1",
        "igcc__cl_fresh|M1",
        "igcc_ccs__cl_fresh|M1",
        "igcc_ccs|M1",
        "igcc|M1",
        "land_use_biomass|M1",
        "landfill_digester1|M1",
        "landfill_direct1|M1",
        "landfill_ele|M1",
        "landfill_heatprdn|M1",
        "lfil_con|M1",
        "lh2_bal|M1",
        "lh2_t_d|M1",
        "liq_bio|M1",
        "loil_cc__air|M1",
        "loil_cc__cl_fresh|M1",
        "loil_cc|M1",
        "loil_conv|M1",
        "loil_exp|M1",
        "loil_fs|M1",
        "loil_imp|M1",
        "loil_i|M1",
        "loil_ppl__air|M1",
        "loil_ppl__cl_fresh|M1",
        "loil_ppl|M1",
        "loil_rc|M1",
        "loil_std|M1",
        "loil_sto|M1",
        "loil_t_d|M1",
        "loil_trade|M1",
        "loil_trp|M1",
        "meth_bal|M1",
        "meth_coal_ccs|M1",
        "meth_coal|M1",
        "meth_fc_trp|M1",
        "meth_ic_trp|M1",
        "meth_i|M1",
        "meth_ng_ccs|M1",
        "meth_ng|M1",
        "meth_rc|M1",
        "meth_t_d|M1",
        "methanol_fs|M1",
        "nica_con|M1",
        "nuc_hc__cl_fresh|M1",
        "nuc_hc|M1",
        "nuc_lc__cl_fresh|M1",
        "nuc_lc|M1",
        "oil_bal|M1",
        "oil_conv|M1",
        "oil_exp|M1",
        "oil_imp|M1",
        "oil_ppl|M1",
        "oil_trade|M1",
        "po_turbine|M1",
        "ref_hil|M1",
        "ref_lol|M1",
        "solar_curtailment1|M1",
        "solar_curtailment2|M1",
        "solar_curtailment3|M1",
        "solar_i|M1",
        "solar_pv_ppl|M1",
        "solar_rc|M1",
        "solar_res1|M1",
        "solar_res2|M1",
        "solar_res3|M1",
        "solar_res4|M1",
        "solar_res5|M1",
        "solar_res6|M1",
        "solar_res7|M1",
        "solar_res8|M1",
        "solar_th_ppl__air|M1",
        "solar_th_ppl__cl_fresh|M1",
        "solar_th_ppl|M1",
        "sp_coal_I|M1",
        "sp_el_I|M1",
        "sp_el_RC|M1",
        "sp_eth_I|M1",
        "sp_liq_I|M1",
        "sp_meth_I|M1",
        "stor_ppl|M1",
        "syn_liq_ccs|M1",
        "syn_liq|M1",
        "wind_curtailment1|M1",
        "wind_curtailment2|M1",
        "wind_curtailment3|M1",
        "wind_ppf|M1",
        "wind_ppl|M1",
        "wind_ref1|M1",
        "wind_ref2|M1",
        "wind_ref3|M1",
        "wind_ref4|M1",
        "wind_ref5|M1",
        "wind_res1|M1",
        "wind_res2|M1",
        "wind_res3|M1",
        "wind_res4|M1",
    ],
}

S_ind = pd.DataFrame(
    0.0,
    index=mapping_ind.keys(),
    columns=sorted(set(v for val in mapping_ind.values() for v in val)),
)
for k, v in mapping_ind.items():
    S_ind.at[k, v] = 1.0

F = pd.read_excel(
    input_folder_1 / "F_mess_conv_2020.xlsx", header=[0, 1], index_col=[0, 1]
)

F.drop("Trade", axis=1, inplace=True)

F = F.transpose().reset_index()
F["region"] = F["region"].str.replace("EUR", "EUR")
F["region"] = F["region"].str.replace("NAM", "NAM")
F["region"] = F["region"].str.replace("LAM", "LAM")
F["region"] = F["region"].str.replace("AFR_ME", "AFR_ME")
F["region"] = F["region"].str.replace("ASI_OZ", "ASI_OZ")

F.set_index(["region", "industry"], inplace=True)
F = F.transpose()


reg = F.columns.levels[0].unique()
S_reg = pd.DataFrame(np.identity(len(reg)), index=reg, columns=reg)

I_i = pd.MultiIndex.from_product(
    [S_reg.index.tolist(), S_ind.index.tolist()], names=["region", "industry"]
)
I_c = pd.MultiIndex.from_product(
    [S_reg.columns.tolist(), S_ind.columns.tolist()],
    names=["region", "industry"],
)
S_I_mess = pd.DataFrame(np.kron(S_reg, S_ind), index=I_i, columns=I_c)

F_agg_mess = F.dot(S_I_mess.transpose())


# create figure

intervention = ("CO2 - combustion - air", "kg")
intervention_mess = ("emis|CO2", "kg")

colors_mess = [
    "#000000",  # coal: "black"
    "#FF6347",  # gas: "tomato"
    "#653700",  # lignite: ""brown
    "#DBB40C",  # oil: "gold"
]

colors_exio = [
    "#00008B",  # Admin : "darkblue"
    "#15B01A",  # Agric : "green"
    "#653700",  # Constr : "brown"
    "#C5C9C7",  # Demand : "silver"
    "#EE82EE",  # Elec :"violett"
    "#FF7F50",  # Heat : "coral"
    "#929591",  # Mining : "grey"
    "#7FFFD4",  # Process : "aquamarine"
    "#FAC205",  # Trans : "goldenrod"
    "#BBF90F",  # Waste : "yellowgreen"
]

colors_hyb = [
    "#00008B",  # Admin : "darkblue"
    "#15B01A",  # Agric : "green"
    "#653700",  # Constr : "brown"
    "#EE82EE",  # Elec :"violett"
    "#FF7F50",  # Heat : "coral"
    "#929591",  # Mining : "grey"
    "#7FFFD4",  # Process : "aquamarine"
    "#FAC205",  # Trans : "goldenrod"
    "#BBF90F",  # Waste : "yellowgreen"
    "#9A0EEA",  # Elec FOR
    "#FC5A50",  # Heat FOR
    "#808080",  # Mining FOR
    "#04D8B2",  # Process FOR
]

# production-based

fig, ((ax2, _ax2), (ax1, _ax1), (ax, _ax)) = plt.subplots(
    nrows=3, ncols=2, figsize=(10, 15), gridspec_kw={"width_ratios": [8, 1]}
)
# hybrid
df = F_agg.loc[[intervention]].transpose().unstack().droplevel([0, 1], axis=1)
df.plot(kind="bar", stacked=True, ax=ax, color=colors_hyb)
ax.set_title("Hybrid system")
ax.set_ylabel(intervention)
ax.set_xticklabels(ax.get_xticklabels(), rotation=0, fontsize=8)
ax.set_ylim(0, 2.35e13)

ax.annotate(
    "Foreground",
    xy=(0.7, 1.62e13),
    xycoords="data",
    xytext=(-0.25, 1.62e13),
    textcoords="data",
    arrowprops=dict(arrowstyle="-[,widthB=4.5", connectionstyle="arc3"),
    va="center",
)

ax.annotate(
    "Background",
    xy=(0.7, 0.56e13),
    xycoords="data",
    xytext=(-0.25, 0.56e13),
    textcoords="data",
    arrowprops=dict(arrowstyle="-[,widthB=5.9", connectionstyle="arc3"),
    va="center",
)

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[::-1], labels[::-1], fontsize=8)

_df = (
    F_agg.loc[[intervention]]
    .transpose()
    .unstack()
    .droplevel([0, 1], axis=1)
    .sum(1)
    .sum()
)
_ax.bar(x=[0], height=_df, color="purple")
_ax.set_ylim(0, 4e13)
_ax.set_xticks([0])
_ax.set_xticklabels(["world"], rotation=0, fontsize=8)
# exiobase
df1 = F_agg_exio.loc[[intervention]].transpose().unstack().droplevel([0, 1], axis=1)
df1.plot(kind="bar", stacked=True, ax=ax1, color=colors_exio)
ax1.set_title("Exiobase")
ax1.set_ylabel(intervention)
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=0, fontsize=8)
ax1.set_ylim(0, 2.35e13)

handles1, labels1 = ax1.get_legend_handles_labels()
ax1.legend(handles1[::-1], labels1[::-1], fontsize=8)

_df1 = (
    F_agg_exio.loc[[intervention]]
    .transpose()
    .unstack()
    .droplevel([0, 1], axis=1)
    .sum(1)
    .sum()
)
_ax1.bar(x=[0], height=_df1, color="red")
_ax1.set_ylim(0, 4e13)
_ax1.set_xticks([0])
_ax1.set_xticklabels(["world"], rotation=0, fontsize=8)
# messageix
df2 = (
    F_agg_mess.loc[[intervention_mess]].transpose().unstack().droplevel([0, 1], axis=1)
)
df2.plot(kind="bar", stacked=True, ax=ax2, color=colors_mess)
ax2.set_title("Messageix (* emissions related to place of extraction)")
ax2.set_ylabel(intervention_mess)
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=0, fontsize=8)
ax2.set_ylim(0, 2.35e13)

handles2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(handles2[::-1], labels2[::-1], fontsize=8)

_df2 = (
    F_agg.loc[[intervention]]
    .transpose()
    .unstack()
    .droplevel([0, 1], axis=1)
    .sum(1)
    .sum()
)
_ax2.bar(x=[0], height=_df2, color="blue")
_ax2.set_ylim(0, 4e13)
_ax2.set_xticks([0])
_ax2.set_xticklabels(["world"], rotation=0, fontsize=8)

plt.subplots_adjust(hspace=0.3)

plt.savefig(output_folder / "Figure_footprint_production_2020.png")


# footprint consumption per commodity
# message
mapping_com = {
    "industry - feedstock": [
        "export|LNG",
        "export|coal",
        "export|crudeoil",
        "export|electr",
        "export|ethanol",
        "export|fueloil",
        "export|lightoil",
        "final|biomass",
        "final|coal",
        "final|d_heat",
        "final|electr",
        "final|ethanol",
        "final|fueloil",
        "final|gas",
        "final|hydrogen",
        "final|lh2",
        "final|lightoil",
        "final|methanol",
        "import|LNG",
        "import|coal",
        "import|crudeoil",
        "import|electr",
        "import|ethanol",
        "import|fueloil",
        "import|lightoil",
        "piped-gas|gas_afr_me",
        "piped-gas|gas_asi_oz",
        "piped-gas|gas_eur",
        "piped-gas|gas_nam",
        "primary|LNG",
        "primary|biomass",
        "primary|coal",
        "primary|crudeoil",
        "primary|dumagr",
        "primary|ethanol",
        "primary|gas",
        "primary|lh2",
        "primary|methanol",
        "secondary|LNG",
        "secondary|coal",
        "secondary|crudeoil",
        "secondary|d_heat",
        "secondary|electr",
        "secondary|ethanol",
        "secondary|exports",
        "secondary|fueloil",
        "secondary|gas",
        "secondary|hydrogen",
        "secondary|lh2",
        "secondary|lightoil",
        "secondary|methanol",
        "secondary|u5",
        "useful|i_feed",
        "useful|non-comm",
    ],
    "industry - specific": ["useful|i_spec"],
    "industry - thermal": ["useful|i_therm"],
    "buildings - specific": ["useful|rc_spec"],
    "buidlings - thermal": ["useful|rc_therm"],
    "transport": ["useful|transport"],
}

F_mess = pd.read_excel(
    input_folder / "footprint_consumption_sector_MESS_per-com_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
F_mess = F_mess.droplevel("unit", axis=1)
S_com = pd.DataFrame(
    0.0,
    index=mapping_com.keys(),
    columns=sorted(set(v for val in mapping_com.values() for v in val)),
)
for k, v in mapping_com.items():
    S_com.at[k, v] = 1.0
reg = F.columns.levels[0].unique()
S_reg = pd.DataFrame(np.identity(len(reg)), index=reg, columns=reg)

I_i = pd.MultiIndex.from_product(
    [S_reg.index.tolist(), S_com.index.tolist()], names=["region", "commodity"]
)
I_c = pd.MultiIndex.from_product(
    [S_reg.columns.tolist(), S_com.columns.tolist()],
    names=["region", "commodity"],
)
S_I_mess = pd.DataFrame(np.kron(S_reg, S_com), index=I_i, columns=I_c)
F_mess.drop("Trade", axis=1, inplace=True)
F_mess = F_mess.transpose().reset_index()
F_mess["region"] = F_mess["region"].str.replace("EUR", "EUR")
F_mess["region"] = F_mess["region"].str.replace("NAM", "NAM")
F_mess["region"] = F_mess["region"].str.replace("LAM", "LAM")
F_mess["region"] = F_mess["region"].str.replace("AFR_ME", "AFR_ME")
F_mess["region"] = F_mess["region"].str.replace("ASI_OZ", "ASI_OZ")
F_mess.set_index(["region", "commodity"], inplace=True)
F_mess = F_mess.transpose()
F_mess = F_mess.dot(S_I_mess.transpose())

# exiobase
F_exio = pd.read_excel(
    input_folder / "footprint_consumption_sector_EXIO_per-com_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
F_exio = F_exio.droplevel("unit", axis=1)
mapping_com = {
    "Agriculture, forestry and fishing products": [
        "Paddy rice",
        "Wheat",
        "Cereal grains nec",
        "Vegetables, fruit, nuts",
        "Oil seeds",
        "Sugar cane, sugar beet",
        "Plant-based fibers",
        "Crops nec",
        "Cattle",
        "Pigs",
        "Poultry",
        "Meat animals nec",
        "Animal products nec",
        "Raw milk",
        "Wool, silk-worm cocoons",
        "Manure (conventional treatment)",
        "Manure (biogas treatment)",
        "Products of forestry, logging and related services (02)",
        "Fish and other fishing products; services incidental of fishing (05)",
    ],
    "Mining and quarrying products": [
        "Anthracite",
        "Coking Coal",
        "Other Bituminous Coal",
        "Sub-Bituminous Coal",
        "Patent Fuel",
        "Lignite/Brown Coal",
        "BKB/Peat Briquettes",
        "Peat",
        "Crude petroleum and services related to crude oil extraction, excluding surveying",
        "Natural gas and services related to natural gas extraction, excluding surveying",
        "Natural Gas Liquids",
        "Other Hydrocarbons",
        "Uranium and thorium ores (12)",
        "Iron ores",
        "Copper ores and concentrates",
        "Nickel ores and concentrates",
        "Aluminium ores and concentrates",
        "Precious metal ores and concentrates",
        "Lead, zinc and tin ores and concentrates",
        "Other non-ferrous metal ores and concentrates",
        "Stone",
        "Sand and clay",
    ],
    "Processing and manufacturing products": [
        "Chemical and fertilizer minerals, salt and other mining and quarrying products n.e.c.",
        "Products of meat cattle",
        "Products of meat pigs",
        "Products of meat poultry",
        "Meat products nec",
        "products of Vegetable oils and fats",
        "Dairy products",
        "Processed rice",
        "Sugar",
        "Food products nec",
        "Beverages",
        "Fish products",
        "Tobacco products (16)",
        "Textiles (17)",
        "Wearing apparel; furs (18)",
        "Leather and leather products (19)",
        "Wood and products of wood and cork (except furniture); articles of straw and plaiting materials (20)",
        "Wood material for treatment, Re-processing of secondary wood material into new wood material",
        "Pulp",
        "Secondary paper for treatment, Re-processing of secondary paper into new pulp",
        "Paper and paper products",
        "Printed matter and recorded media (22)",
        "Coke Oven Coke",
        "Gas Coke",
        "Coal Tar",
        "Motor Gasoline",
        "Aviation Gasoline",
        "Gasoline Type Jet Fuel",
        "Kerosene Type Jet Fuel",
        "Kerosene",
        "Gas/Diesel Oil",
        "Heavy Fuel Oil",
        "Refinery Gas",
        "Liquefied Petroleum Gases (LPG)",
        "Refinery Feedstocks",
        "Ethane",
        "Naphtha",
        "White Spirit & SBP",
        "Lubricants",
        "Bitumen",
        "Paraffin Waxes",
        "Petroleum Coke",
        "Non-specified Petroleum Products",
        "Nuclear fuel",
        "Plastics, basic",
        "Secondary plastic for treatment, Re-processing of secondary plastic into new plastic",
        "N-fertiliser",
        "P- and other fertiliser",
        "Chemicals nec",
        "Charcoal",
        "Additives/Blending Components",
        "Biogasoline",
        "Biodiesels",
        "Other Liquid Biofuels",
        "Rubber and plastic products (25)",
        "Glass and glass products",
        "Secondary glass for treatment, Re-processing of secondary glass into new glass",
        "Ceramic goods",
        "Bricks, tiles and construction products, in baked clay",
        "Cement, lime and plaster",
        "Ash for treatment, Re-processing of ash into clinker",
        "Other non-metallic mineral products",
        "Basic iron and steel and of ferro-alloys and first products thereof",
        "Secondary steel for treatment, Re-processing of secondary steel into new steel",
        "Precious metals",
        "Secondary preciuos metals for treatment, Re-processing of secondary preciuos metals into new preciuos metals",
        "Aluminium and aluminium products",
        "Secondary aluminium for treatment, Re-processing of secondary aluminium into new aluminium",
        "Lead, zinc and tin and products thereof",
        "Secondary lead for treatment, Re-processing of secondary lead into new lead",
        "Copper products",
        "Secondary copper for treatment, Re-processing of secondary copper into new copper",
        "Other non-ferrous metal products",
        "Secondary other non-ferrous metals for treatment, Re-processing of secondary other non-ferrous metals into new other non-ferrous metals",
        "Foundry work services",
        "Fabricated metal products, except machinery and equipment (28)",
        "Machinery and equipment n.e.c. (29)",
        "Office machinery and computers (30)",
        "Electrical machinery and apparatus n.e.c. (31)",
        "Radio, television and communication equipment and apparatus (32)",
        "Medical, precision and optical instruments, watches and clocks (33)",
        "Motor vehicles, trailers and semi-trailers (34)",
        "Other transport equipment (35)",
        "Furniture; other manufactured goods n.e.c. (36)",
        "Secondary raw materials",
        "Bottles for treatment, Recycling of bottles by direct reuse",
    ],
    "Electricity": [
        "Electricity by coal",
        "Electricity by gas",
        "Electricity by nuclear",
        "Electricity by hydro",
        "Electricity by wind",
        "Electricity by petroleum and other oil derivatives",
        "Electricity by biomass and waste",
        "Electricity by solar photovoltaic",
        "Electricity by solar thermal",
        "Electricity by tide, wave, ocean",
        "Electricity by Geothermal",
        "Electricity nec",
        "Transmission services of electricity",
        "Distribution and trade services of electricity",
    ],
    "Heat and water": [
        "Coke oven gas",
        "Blast Furnace Gas",
        "Oxygen Steel Furnace Gas",
        "Gas Works Gas",
        "Biogas",
        "Distribution services of gaseous fuels through mains",
        "Steam and hot water supply services",
        "Collected and purified water, distribution services of water (41)",
    ],
    "Construction": [
        "Construction work (45)",
        "Secondary construction material for treatment, Re-processing of secondary construction material into aggregates",
    ],
    "Administrative, support and service products": [
        "Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries",
        "Retail trade services of motor fuel",
        "Wholesale trade and commission trade services, except of motor vehicles and motorcycles (51)",
        "Retail  trade services, except of motor vehicles and motorcycles; repair services of personal and household goods (52)",
        "Hotel and restaurant services (55)",
        "Supporting and auxiliary transport services; travel agency services (63)",
        "Post and telecommunication services (64)",
        "Financial intermediation services, except insurance and pension funding services (65)",
        "Insurance and pension funding services, except compulsory social security services (66)",
        "Services auxiliary to financial intermediation (67)",
        "Real estate services (70)",
        "Renting services of machinery and equipment without operator and of personal and household goods (71)",
        "Computer and related services (72)",
        "Research and development services (73)",
        "Other business services (74)",
        "Public administration and defence services; compulsory social security services (75)",
        "Education services (80)",
        "Health and social work services (85)",
        "Membership organisation services n.e.c. (91)",
        "Recreational, cultural and sporting services (92)",
        "Other services (93)",
        "Private households with employed persons (95)",
        "Extra-territorial organizations and bodies",
    ],
    "Transport": [
        "Railway transportation services",
        "Other land transportation services",
        "Transportation services via pipelines",
        "Sea and coastal water transportation services",
        "Inland water transportation services",
        "Air transport services (62)",
    ],
    "Waste": [
        "Food waste for treatment: incineration",
        "Paper waste for treatment: incineration",
        "Plastic waste for treatment: incineration",
        "Intert/metal waste for treatment: incineration",
        "Textiles waste for treatment: incineration",
        "Wood waste for treatment: incineration",
        "Oil/hazardous waste for treatment: incineration",
        "Food waste for treatment: biogasification and land application",
        "Paper waste for treatment: biogasification and land application",
        "Sewage sludge for treatment: biogasification and land application",
        "Food waste for treatment: composting and land application",
        "Paper and wood waste for treatment: composting and land application",
        "Food waste for treatment: waste water treatment",
        "Other waste for treatment: waste water treatment",
        "Food waste for treatment: landfill",
        "Paper for treatment: landfill",
        "Plastic waste for treatment: landfill",
        "Inert/metal/hazardous waste for treatment: landfill",
        "Textiles waste for treatment: landfill",
        "Wood waste for treatment: landfill",
    ],
    "Demand": [
        "Final consumption expenditure by households",
        "Final consumption expenditure by non-profit organisations serving households (NPISH)",
        "Final consumption expenditure by government",
        "Gross fixed capital formation",
        "Changes in inventories",
        "Changes in valuables",
        "Exports: Total (fob)",
    ],
    "Demand": [
        "Final consumption expenditure by households",
        "Final consumption expenditure by non-profit organisations serving households (NPISH)",
        "Final consumption expenditure by government",
        "Gross fixed capital formation",
        "Changes in inventories",
        "Changes in valuables",
        "Exports: Total (fob)",
    ],
}

S_com = pd.DataFrame(
    0.0,
    index=mapping_com.keys(),
    columns=sorted(set(v for val in mapping_com.values() for v in val)),
)
for k, v in mapping_com.items():
    S_com.at[k, v] = 1.0
reg = F.columns.levels[0].unique()
S_reg = pd.DataFrame(np.identity(len(reg)), index=reg, columns=reg)

I_i = pd.MultiIndex.from_product(
    [S_reg.index.tolist(), S_com.index.tolist()], names=["region", "commodity"]
)
I_c = pd.MultiIndex.from_product(
    [S_reg.columns.tolist(), S_com.columns.tolist()],
    names=["region", "commodity"],
)
S_I_exio = pd.DataFrame(np.kron(S_reg, S_com), index=I_i, columns=I_c)
F_exio = F_exio.transpose().reset_index()
F_exio["region"] = F_exio["region"].str.replace("europe", "EUR")
F_exio["region"] = F_exio["region"].str.replace("north_america", "NAM")
F_exio["region"] = F_exio["region"].str.replace("latin_america", "LAM")
F_exio["region"] = F_exio["region"].str.replace("africa_mid_east", "AFR_ME")
F_exio["region"] = F_exio["region"].str.replace("asia_ozeania", "ASI_OZ")
F_exio.set_index(["region", "commodity"], inplace=True)
F_exio = F_exio.transpose()
F_exio = F_exio.dot(S_I_exio.transpose())

# hybrid
F_hyb = pd.read_excel(
    input_folder / "footprint_consumption_sector_HYB_per-com_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
F_hyb = F_hyb.droplevel("unit", axis=1)
mapping_com = {
    "FOR: Mining and quarrying products": [
        "primary|coal",
        "primary|crudeoil",
        "primary|gas",
    ],
    "FOR: Processing and manufacturing products": [
        "export|LNG",
        "export|coal",
        "export|crudeoil",
        "export|ethanol",
        "export|fueloil",
        "export|lightoil",
        "final|biomass",
        "final|coal",
        "final|ethanol",
        "final|fueloil",
        "final|gas",
        "final|hydrogen",
        "final|lh2",
        "final|lightoil",
        "final|methanol",
        "import|LNG",
        "import|coal",
        "import|crudeoil",
        "import|ethanol",
        "import|fueloil",
        "import|lightoil",
        "piped-gas|gas_afr_me",
        "piped-gas|gas_asi_oz",
        "piped-gas|gas_eur",
        "piped-gas|gas_nam",
        "primary|LNG",
        "primary|biomass",
        "primary|dumagr",
        "primary|ethanol",
        "primary|lh2",
        "primary|methanol",
        "secondary|LNG",
        "secondary|coal",
        "secondary|crudeoil",
        "secondary|ethanol",
        "secondary|exports",
        "secondary|fueloil",
        "secondary|gas",
        "secondary|hydrogen",
        "secondary|lh2",
        "secondary|lightoil",
        "secondary|methanol",
        "secondary|u5",
        "useful|i_feed",
        "useful|transport",
        "useful|non-comm",
    ],
    "FOR: Electricity": [
        "secondary|electr",
        "import|electr",
        "final|electr",
        "export|electr",
        "useful|i_spec",
        "useful|rc_spec",
    ],
    "FOR: Heat and water": [
        "final|d_heat",
        "secondary|d_heat",
        "useful|i_therm",
        "useful|rc_therm",
    ],
    "BACK: Agriculture, forestry and fishing products": [
        "Paddy rice",
        "Wheat",
        "Cereal grains nec",
        "Vegetables, fruit, nuts",
        "Oil seeds",
        "Sugar cane, sugar beet",
        "Plant-based fibers",
        "Crops nec",
        "Cattle",
        "Pigs",
        "Poultry",
        "Meat animals nec",
        "Animal products nec",
        "Raw milk",
        "Wool, silk-worm cocoons",
        "Manure (conventional treatment)",
        "Manure (biogas treatment)",
        "Products of forestry, logging and related services (02)",
        "Fish and other fishing products; services incidental of fishing (05)",
    ],
    "BACK: Mining and quarrying products": [
        "Anthracite",
        "Coking Coal",
        "Other Bituminous Coal",
        "Sub-Bituminous Coal",
        "Patent Fuel",
        "Lignite/Brown Coal",
        "BKB/Peat Briquettes",
        "Peat",
        "Crude petroleum and services related to crude oil extraction, excluding surveying",
        "Natural gas and services related to natural gas extraction, excluding surveying",
        "Natural Gas Liquids",
        "Other Hydrocarbons",
        "Uranium and thorium ores (12)",
        "Iron ores",
        "Copper ores and concentrates",
        "Nickel ores and concentrates",
        "Aluminium ores and concentrates",
        "Precious metal ores and concentrates",
        "Lead, zinc and tin ores and concentrates",
        "Other non-ferrous metal ores and concentrates",
        "Stone",
        "Sand and clay",
    ],
    "BACK: Processing and manufacturing products": [
        "Chemical and fertilizer minerals, salt and other mining and quarrying products n.e.c.",
        "Products of meat cattle",
        "Products of meat pigs",
        "Products of meat poultry",
        "Meat products nec",
        "products of Vegetable oils and fats",
        "Dairy products",
        "Processed rice",
        "Sugar",
        "Food products nec",
        "Beverages",
        "Fish products",
        "Tobacco products (16)",
        "Textiles (17)",
        "Wearing apparel; furs (18)",
        "Leather and leather products (19)",
        "Wood and products of wood and cork (except furniture); articles of straw and plaiting materials (20)",
        "Wood material for treatment, Re-processing of secondary wood material into new wood material",
        "Pulp",
        "Secondary paper for treatment, Re-processing of secondary paper into new pulp",
        "Paper and paper products",
        "Printed matter and recorded media (22)",
        "Coke Oven Coke",
        "Gas Coke",
        "Coal Tar",
        "Motor Gasoline",
        "Aviation Gasoline",
        "Gasoline Type Jet Fuel",
        "Kerosene Type Jet Fuel",
        "Kerosene",
        "Gas/Diesel Oil",
        "Heavy Fuel Oil",
        "Refinery Gas",
        "Liquefied Petroleum Gases (LPG)",
        "Refinery Feedstocks",
        "Ethane",
        "Naphtha",
        "White Spirit & SBP",
        "Lubricants",
        "Bitumen",
        "Paraffin Waxes",
        "Petroleum Coke",
        "Non-specified Petroleum Products",
        "Nuclear fuel",
        "Plastics, basic",
        "Secondary plastic for treatment, Re-processing of secondary plastic into new plastic",
        "N-fertiliser",
        "P- and other fertiliser",
        "Chemicals nec",
        "Charcoal",
        "Additives/Blending Components",
        "Biogasoline",
        "Biodiesels",
        "Other Liquid Biofuels",
        "Rubber and plastic products (25)",
        "Glass and glass products",
        "Secondary glass for treatment, Re-processing of secondary glass into new glass",
        "Ceramic goods",
        "Bricks, tiles and construction products, in baked clay",
        "Cement, lime and plaster",
        "Ash for treatment, Re-processing of ash into clinker",
        "Other non-metallic mineral products",
        "Basic iron and steel and of ferro-alloys and first products thereof",
        "Secondary steel for treatment, Re-processing of secondary steel into new steel",
        "Precious metals",
        "Secondary preciuos metals for treatment, Re-processing of secondary preciuos metals into new preciuos metals",
        "Aluminium and aluminium products",
        "Secondary aluminium for treatment, Re-processing of secondary aluminium into new aluminium",
        "Lead, zinc and tin and products thereof",
        "Secondary lead for treatment, Re-processing of secondary lead into new lead",
        "Copper products",
        "Secondary copper for treatment, Re-processing of secondary copper into new copper",
        "Other non-ferrous metal products",
        "Secondary other non-ferrous metals for treatment, Re-processing of secondary other non-ferrous metals into new other non-ferrous metals",
        "Foundry work services",
        "Fabricated metal products, except machinery and equipment (28)",
        "Machinery and equipment n.e.c. (29)",
        "Office machinery and computers (30)",
        "Electrical machinery and apparatus n.e.c. (31)",
        "Radio, television and communication equipment and apparatus (32)",
        "Medical, precision and optical instruments, watches and clocks (33)",
        "Motor vehicles, trailers and semi-trailers (34)",
        "Other transport equipment (35)",
        "Furniture; other manufactured goods n.e.c. (36)",
        "Secondary raw materials",
        "Bottles for treatment, Recycling of bottles by direct reuse",
    ],
    "BACK: Electricity": [
        "Electricity by coal",
        "Electricity by gas",
        "Electricity by nuclear",
        "Electricity by hydro",
        "Electricity by wind",
        "Electricity by petroleum and other oil derivatives",
        "Electricity by biomass and waste",
        "Electricity by solar photovoltaic",
        "Electricity by solar thermal",
        "Electricity by tide, wave, ocean",
        "Electricity by Geothermal",
        "Electricity nec",
        "Transmission services of electricity",
        "Distribution and trade services of electricity",
    ],
    "BACK: Heat and water": [
        "Coke oven gas",
        "Blast Furnace Gas",
        "Oxygen Steel Furnace Gas",
        "Gas Works Gas",
        "Biogas",
        "Distribution services of gaseous fuels through mains",
        "Steam and hot water supply services",
        "Collected and purified water, distribution services of water (41)",
    ],
    "BACK: Construction": [
        "Construction work (45)",
        "Secondary construction material for treatment, Re-processing of secondary construction material into aggregates",
    ],
    "BACK: Administrative, support and service products": [
        "Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries",
        "Retail trade services of motor fuel",
        "Wholesale trade and commission trade services, except of motor vehicles and motorcycles (51)",
        "Retail  trade services, except of motor vehicles and motorcycles; repair services of personal and household goods (52)",
        "Hotel and restaurant services (55)",
        "Supporting and auxiliary transport services; travel agency services (63)",
        "Post and telecommunication services (64)",
        "Financial intermediation services, except insurance and pension funding services (65)",
        "Insurance and pension funding services, except compulsory social security services (66)",
        "Services auxiliary to financial intermediation (67)",
        "Real estate services (70)",
        "Renting services of machinery and equipment without operator and of personal and household goods (71)",
        "Computer and related services (72)",
        "Research and development services (73)",
        "Other business services (74)",
        "Public administration and defence services; compulsory social security services (75)",
        "Education services (80)",
        "Health and social work services (85)",
        "Membership organisation services n.e.c. (91)",
        "Recreational, cultural and sporting services (92)",
        "Other services (93)",
        "Private households with employed persons (95)",
        "Extra-territorial organizations and bodies",
    ],
    "BACK: Transport": [
        "Railway transportation services",
        "Other land transportation services",
        "Transportation services via pipelines",
        "Sea and coastal water transportation services",
        "Inland water transportation services",
        "Air transport services (62)",
    ],
    "BACK: Waste": [
        "Food waste for treatment: incineration",
        "Paper waste for treatment: incineration",
        "Plastic waste for treatment: incineration",
        "Intert/metal waste for treatment: incineration",
        "Textiles waste for treatment: incineration",
        "Wood waste for treatment: incineration",
        "Oil/hazardous waste for treatment: incineration",
        "Food waste for treatment: biogasification and land application",
        "Paper waste for treatment: biogasification and land application",
        "Sewage sludge for treatment: biogasification and land application",
        "Food waste for treatment: composting and land application",
        "Paper and wood waste for treatment: composting and land application",
        "Food waste for treatment: waste water treatment",
        "Other waste for treatment: waste water treatment",
        "Food waste for treatment: landfill",
        "Paper for treatment: landfill",
        "Plastic waste for treatment: landfill",
        "Inert/metal/hazardous waste for treatment: landfill",
        "Textiles waste for treatment: landfill",
        "Wood waste for treatment: landfill",
    ],
}
S_com = pd.DataFrame(
    0.0,
    index=mapping_com.keys(),
    columns=sorted(set(v for val in mapping_com.values() for v in val)),
)
for k, v in mapping_com.items():
    S_com.at[k, v] = 1.0
reg = F.columns.levels[0].unique()
S_reg = pd.DataFrame(np.identity(len(reg)), index=reg, columns=reg)

I_i = pd.MultiIndex.from_product(
    [S_reg.index.tolist(), S_com.index.tolist()], names=["region", "commodity"]
)
I_c = pd.MultiIndex.from_product(
    [S_reg.columns.tolist(), S_com.columns.tolist()],
    names=["region", "commodity"],
)
S_I_hyb = pd.DataFrame(np.kron(S_reg, S_com), index=I_i, columns=I_c)
F_hyb.drop("Trade", axis=1, inplace=True)
F_hyb = F_hyb.transpose().reset_index()
F_hyb["region"] = F_hyb["region"].str.replace("europe", "EUR")
F_hyb["region"] = F_hyb["region"].str.replace("north_america", "NAM")
F_hyb["region"] = F_hyb["region"].str.replace("latin_america", "LAM")
F_hyb["region"] = F_hyb["region"].str.replace("africa_mid_east", "AFR_ME")
F_hyb["region"] = F_hyb["region"].str.replace("asia_ozeania", "ASI_OZ")
F_hyb.set_index(["region", "commodity"], inplace=True)
F_hyb = F_hyb.transpose()
F_hyb = F_hyb.dot(S_I_hyb.transpose())


colors_mess = [
    "#069AF3",  # buikdings th: "azure"
    "#00FFFF",  # buildings sp: "aqua"
    "#DAA520",  # industry fs: "goldenrod"
    "#FFD700",  # industry sp: "gold"
    "#FFA500",  # industry th: 'orange'
    "#DDA0DD",  # transport: 'plum'
]

fig, ((ax2, _ax2), (ax1, _ax1), (ax, _ax)) = plt.subplots(
    nrows=3, ncols=2, figsize=(10, 15), gridspec_kw={"width_ratios": [8, 1]}
)

df = F_hyb.loc[[intervention]].transpose().unstack().droplevel([0, 1], axis=1)
df.plot(kind="bar", stacked=True, ax=ax, color=colors_hyb)
ax.set_title("Hybrid system")
ax.set_ylabel(intervention)
ax.set_xticklabels(ax.get_xticklabels(), rotation=0, fontsize=8)
ax.set_ylim(0, 2.35e13)

ax.annotate(
    "Foreground",
    xy=(0.7, 1.95e13),
    xycoords="data",
    xytext=(-0.3, 1.95e13),
    textcoords="data",
    arrowprops=dict(arrowstyle="-[,widthB=2.7", connectionstyle="arc3"),
    va="center",
)

ax.annotate(
    "Background",
    xy=(0.7, 0.79e13),
    xycoords="data",
    xytext=(-0.3, 0.79e13),
    textcoords="data",
    arrowprops=dict(arrowstyle="-[,widthB=8.48", connectionstyle="arc3"),
    va="center",
)

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[::-1], labels[::-1], fontsize=8)

_df = (
    F_hyb.loc[[intervention]]
    .transpose()
    .unstack()
    .droplevel([0, 1], axis=1)
    .sum(1)
    .sum()
)
_ax.bar(x=[0], height=_df, color="purple")
_ax.set_ylim(0, 4e13)
_ax.set_xticks([0])
_ax.set_xticklabels(["world"], rotation=0, fontsize=8)

df1 = F_exio.loc[[intervention]].transpose().unstack().droplevel([0, 1], axis=1)
df1.plot(kind="bar", stacked=True, ax=ax1, color=colors_exio)
ax1.set_title("Exiobase")
ax1.set_ylabel(intervention)
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=0, fontsize=8)
ax1.set_ylim(0, 2.35e13)

handles1, labels1 = ax1.get_legend_handles_labels()
ax1.legend(handles1[::-1], labels1[::-1], fontsize=8)

_df1 = (
    F_exio.loc[[intervention]]
    .transpose()
    .unstack()
    .droplevel([0, 1], axis=1)
    .sum(1)
    .sum()
)
_ax1.bar(x=[0], height=_df1, color="red")
_ax1.set_ylim(0, 4e13)
_ax1.set_xticks([0])
_ax1.set_xticklabels(["world"], rotation=0, fontsize=8)

df2 = F_mess.loc[[intervention_mess]].transpose().unstack().droplevel([0, 1], axis=1)
df2.plot(kind="bar", stacked=True, ax=ax2, color=colors_mess)
ax2.set_title("Messageix")
ax2.set_ylabel(intervention_mess)
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=0, fontsize=8)
ax2.set_ylim(0, 2.35e13)

handles2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(handles2[::-1], labels2[::-1], fontsize=8)

_df2 = (
    F_mess.loc[[intervention_mess]]
    .transpose()
    .unstack()
    .droplevel([0, 1], axis=1)
    .sum(1)
    .sum()
)
_ax2.bar(x=[0], height=_df2, color="blue")
_ax2.set_ylim(0, 4e13)
_ax2.set_xticks([0])
_ax2.set_xticklabels(["world"], rotation=0, fontsize=8)

plt.subplots_adjust(hspace=0.3)

plt.savefig(output_folder / "Figure_footprint_consumption_per-com_2020.png")
