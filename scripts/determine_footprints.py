from pathlib import Path
import os

result_folder = Path(__file__).parent / "../output_results"
data_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(result_folder):
    os.makedirs(result_folder)

if not os.path.exists(data_folder):
    os.makedirs(data_folder)

import hybridvut as hyb
import pandas as pd


# HYBRID

V = pd.read_excel(
    result_folder / "V_hybridized_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
U = pd.read_excel(
    result_folder / "U_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1, 2],
)
F = pd.read_excel(
    result_folder / "F_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


VUT_total = hyb.VUT(V=V, U=U, F=F)


# footprints
footprint_consumption = VUT_total.calc_footprint(
    responsibility="consumption",
    raw_factors="per-ind",
    multipliers="per-com",
    sum_over_sectors=False,
)
footprint_consumption.to_excel(
    result_folder / "footprint_consumption_sector_HYB_per-com_2020.xlsx"
)

footprint_consumption = VUT_total.calc_footprint(
    responsibility="consumption",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_consumption.to_excel(
    result_folder / "footprint_consumption_sector_HYB_2020.xlsx"
)

footprint_income = VUT_total.calc_footprint(
    responsibility="income",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_income.to_excel(result_folder / "footprint_income_sector_HYB_2020.xlsx")

footprint_production = VUT_total.calc_footprint(
    responsibility="production",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_production.to_excel(
    result_folder / "footprint_production_sector_HYB_2020.xlsx"
)


# MESSAGE

V = pd.read_excel(
    data_folder / "V_for_agg_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
U = pd.read_excel(
    data_folder / "U_for_agg_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1, 2],
)
F = pd.read_excel(
    data_folder / "F_Mess_conv_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


VUT_total = hyb.VUT(V=V, U=U, F=F)


# footprints
footprint_consumption = VUT_total.calc_footprint(
    responsibility="consumption",
    raw_factors="per-ind",
    multipliers="per-com",
    sum_over_sectors=False,
)
footprint_consumption.to_excel(
    result_folder / "footprint_consumption_sector_MESS_per-com_2020.xlsx"
)

footprint_consumption = VUT_total.calc_footprint(
    responsibility="consumption",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_consumption.to_excel(
    result_folder / "footprint_consumption_sector_MESS_2020.xlsx"
)

footprint_income = VUT_total.calc_footprint(
    responsibility="income",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_income.to_excel(result_folder / "footprint_income_sector_MESS_2020.xlsx")

footprint_production = VUT_total.calc_footprint(
    responsibility="production",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_production.to_excel(
    result_folder / "footprint_production_sector_MESS_2020.xlsx"
)


# EXIOBASE

V = pd.read_excel(
    data_folder / "V_back_agg_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
U = pd.read_excel(
    data_folder / "U_back_agg_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1, 2],
)
F = pd.read_excel(
    data_folder / "F_Exio_conv_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)
F_Y = pd.read_excel(
    data_folder / "F_Y_Exio_conv_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


VUT_total = hyb.VUT(V=V, U=U, F=F, F_Y=F_Y)


# footprints
footprint_consumption = VUT_total.calc_footprint(
    responsibility="consumption",
    raw_factors="per-ind",
    multipliers="per-com",
    sum_over_sectors=False,
)
footprint_consumption.to_excel(
    result_folder / "footprint_consumption_sector_EXIO_per-com_2020.xlsx"
)

footprint_consumption = VUT_total.calc_footprint(
    responsibility="consumption",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_consumption.to_excel(
    result_folder / "footprint_consumption_sector_EXIO_2020.xlsx"
)

footprint_income = VUT_total.calc_footprint(
    responsibility="income",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_income.to_excel(result_folder / "footprint_income_sector_EXIO_2020.xlsx")

footprint_production = VUT_total.calc_footprint(
    responsibility="production",
    raw_factors="per-ind",
    multipliers="per-ind",
    sum_over_sectors=False,
)
footprint_production.to_excel(
    result_folder / "footprint_production_sector_EXIO_2020.xlsx"
)
